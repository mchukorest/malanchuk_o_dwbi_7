use product_storage
go

set transaction isolation level repeatable read

begin transaction

select * from dbo.description

---- other statements
---- wait, another transaction updates data
waitfor delay '00:00:10'
---- read data again

select * from dbo.description

commit transaction

---- rollback changes
update dbo.description 
set description = '�������' 
where id = 1
