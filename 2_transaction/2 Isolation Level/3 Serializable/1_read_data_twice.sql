use product_storage
go

set transaction isolation level serializable

begin transaction

select * 
from dbo.office  
where id > 100

---- other statements
---- wait, another transaction insert row
waitfor delay '00:00:10'
---- read data again

select * 
from dbo.office  
where id > 100

commit transaction

---- rollback changes
delete from  dbo.office 
where id = 100
