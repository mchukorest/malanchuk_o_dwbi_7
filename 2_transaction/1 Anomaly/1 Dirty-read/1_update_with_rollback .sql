-- first transaction updada data
use product_storage
go

begin transaction

update dbo.payment
set pay_system = 'MasterCaard' 
where id = 1001

--wait, first transaction read uncommitted data

waitfor delay '00:00:05'

rollback transaction