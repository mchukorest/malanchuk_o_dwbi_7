use product_storage
go


begin transaction

select * 
from dbo.payment  
where id > 1003

---- other statements
---- wait, another transaction inserts new row
waitfor delay '00:00:10'
---- read data again


select * 
from dbo.payment  
where id > 1003

commit transaction

---- rollback changes
delete from  dbo.payment
 
where id = 1003
