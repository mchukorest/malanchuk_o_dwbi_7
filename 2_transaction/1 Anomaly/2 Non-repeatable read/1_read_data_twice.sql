use product_storage
go

begin transaction

select * from dbo.payment

---- o ... s
---- wait, another transaction updates data
waitfor delay '00:00:10'
---- read data again

select * from dbo.payment

commit transaction

---- rollback changes
update dbo.payment 
set pay_system = 'VISA' 
where id = 1
