-- second transactions updates data
use product_storage
go

begin transaction

update dbo.payment 
set pay_system = 'MasterCard' 
where id = 1

commit transaction