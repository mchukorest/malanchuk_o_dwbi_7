-- transaction b
use product_storage
go

begin transaction

update dbo.payment set pay_system = 'VISA 2' where id = 1001

waitfor delay '00:00:05'
--- Transaction A continue

-- try to upgate 
update dbo.payment set pay_system = 'VISA 3' where id = 1002

commit transaction