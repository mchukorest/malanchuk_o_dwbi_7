-- transaction A
use product_storage
go

begin transaction

update dbo.payment set pay_system = 'VISA 3' where id = 1002

waitfor delay '00:00:05'
--- Transaction B started

-- try to Update
update dbo.payment set pay_system = 'VISA 2' where id = 1001

commit transaction