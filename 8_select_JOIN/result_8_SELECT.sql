use labor_sql; 
go

--1.  �� �����. �����. ������ ��������� ��� ��� ��, �� ����� 
--�������� ���� �ᒺ��� �� ����� 8 �����. �������: maker, 
--type, speed, hd.
SELECT maker, type, speed, hd
FROM  pc
INNER JOIN product ON pc.model = product.model
where hd <= 8

--2.  �� �����. �����. ������� ��������� �� � ���������� �� 
--����� 600 ���. �������: maker.
SELECT maker,  speed
FROM  pc
INNER JOIN product ON pc.model = product.model
where speed >= 600

--3.  �� �����. �����. ������� ��������� �������� � 
--���������� �� ���� 500 ���. �������: maker.
SELECT maker,  speed
FROM  laptop
INNER JOIN product ON laptop.model = product.model
where speed <= 500

--4.  �� �����. �����. ������� ���� ������� ��������, �� 
--����� ������� �ᒺ�� �������� ����� �� RAM (������� 
--Laptop). � ��������� ����� ���� ���������� ���� ���� ���. 
--������� ���������: ������ � ������ �������, ������ � 
--������ �������, �ᒺ� ����� �� RAM.
SELECT  maker, hd, ram
FROM  laptop
INNER JOIN product ON laptop.model = product.model

--5.  �� ������볻. ������� �����, �� ���� ����� �� ��������� 
--������� ������� 'bb', ��� � ����� �������� 'bc'. �������: 
--country, ���� � ������ 'bb', ���� � ������ 'bc'.
SELECT  country, type
FROM  classes
INNER JOIN ships ON classes.class = ships.class
where type = 'bc' or type = 'bb'


--6.  �� �����. �����. ������� ����� ����� �� ��������� ��, 
--��� �� ���� ����� �� 600 ���. �������: model, maker.
SELECT maker , product.model
FROM  pc
INNER JOIN product ON pc.model = product.model
where price < 600

--7.  �� �����. �����. ������� ����� ����� �� ��������� 
--�������, ��� �� ���� ���� �� 300 ���. �������: model, maker.
SELECT maker , product.model
FROM  printer
INNER JOIN product ON printer.model = product.model
where price > 300

--8.  �� �����. �����. ������� ���������, ����� ����� �� ���� 
--������� ���������, �� � � ��. �������: maker, model, price.
SELECT maker , product.model , price
FROM  pc
INNER JOIN product ON pc.model = product.model

--9.  �� �����. �����. ������� �� ������ ����� ��, �� 
--��������� �� ���� (���� ���� �������). �������: maker, 
--model, price.
SELECT maker , product.model , price
FROM  pc
INNER JOIN product ON pc.model = product.model
where price is not null

--10.  �� �����. �����. ������� ���������, ���, ������ �� ������� 
--��������� ��� ��������, ������� ��������� ���� �������� 
--600 ���. �������: maker, type, model, speed.
SELECT maker,  type, laptop.model, speed
FROM  laptop
INNER JOIN product ON laptop.model = product.model
where speed > 600

--11.  �� ������볻. ��� ������� ������� Ships ������� �� 
--���������������.
SELECT  displacement, name , ships.class, launched
FROM  classes
INNER JOIN ships ON classes.class = ships.class

--12.  �� ������볻. ��� �������, �� ������ � ������, ������� 
--����� �� ���� ����, � ���� ���� ����� ������.
SELECT battle, date, ship
FROM  battles
INNER JOIN outcomes ON battles.name = outcomes.battle
where result != 'sunk'

--13.  �� ������볻. ��� ������� ������� Ships ������� �����, 
--���� ���� ��������.
SELECT  country, name , ships.class, launched
FROM  classes
INNER JOIN ships ON classes.class = ships.class

--14.  �� ���������. ��� �������� ����� 'Boeing' ������� ����� 
--��������, ���� ���� ��������.
SELECT name, plane
FROM  trip
INNER JOIN company ON trip.id_comp = company.id_comp
where plane = 'boeing'

--15.  �� ���������. ��� �������� ������� Passenger ������� 
--����, ���� ���� ������������� ��������� �������.
SELECT name, date
FROM  pass_in_trip
INNER JOIN passenger ON pass_in_trip.id_psg = passenger.id_psg

--16.  �� �����. �����. ������ ������, ������� ��������� �� �ᒺ� 
--��������� ����� ��� ��� ���������, �� �������������� 
--�������������� 10 ��� 20 �� �� ������������ ���������� 
--'A'. �������: model, speed, hd. ������� ���� ������������ �� 
--���������� �� �������� speed.
SELECT maker, product.model, speed, hd
FROM  pc
INNER JOIN product ON pc.model = product.model
where (hd = 10 or hd = 20) and maker = 'A'
order by speed ASC

--17.  ��� ������� ��������� � ������� Product ��������� ����� 
--������� ������� ���� ��������� (PIVOT).
SELECT *
FROM  (select product.maker,type from product)
AS info1
PIVOT( Count(type)
FOR type IN ([pc], [laptop], [printer]) ) AS PIVOT1;

--18.  ���������� ������� ���� �� �������� � ��������� �� 
--������ ������. (PIVOT)
SELECT *
FROM  (select price ,screen from laptop)
AS info2
PIVOT( AVG([price]) 
FOR screen IN ([11],[12],[14],[15]) ) AS PIVOT2;

--19.  ��� ������� �������� ��������� ������� ��� ���������. 
--(CROSS APPLY)

SELECT * FROM laptop L
CROSS APPLY (SELECT maker FROM  product P
WHERE L.model = P.model ) P 

--20.  ��� ������� �������� ��������� ������� ����������� ���� 
--����� �������� ���� � ���������. (CROSS APPLY)
 SELECT * FROM laptop L1
 CROSS APPLY (SELECT MAX(price) max_price  FROM Laptop L2
 JOIN  product P ON L2.model=P.model 
 WHERE maker = (SELECT maker FROM product P2 WHERE P2.model= L1.model)) L

--21.  Ǻ����� ����� ����� � ������� Laptop � ���������  ������ � 
--�������, �������� ����������� (model, code). (CROSS 
--APPLY) 
 SELECT * FROM laptop L1
 CROSS APPLY (SELECT TOP 1 * FROM Laptop L2 
 WHERE L1.model < L2.model OR (L1.model = L2.model AND L1.code < L2.code) 
 ORDER BY model, code) L  ORDER BY L1.model

--22.  �'������ ����� ����� � ������� Laptop � ���������  ������ � 
--�������, �������� ����������� (model, code). (OUTER 
--APPLY)
SELECT * FROM laptop L1
OUTER APPLY (SELECT TOP 1 * FROM Laptop L2 
WHERE L1.model < L2.model OR (L1.model = L2.model AND L1.code < L2.code) 
ORDER BY model, code) L ORDER BY L1.model

--23.  ������� � ������� Product �� ��� ����� � ���������� 
--�������� � ����� �����, ��� ��������������� ����� 
--���������. (CROOSS APPLY)
SELECT T.* FROM (SELECT DISTINCT type FROM product) P 
CROSS APPLY (SELECT TOP 3 * FROM product P2 WHERE  P.type=P2.type ORDER BY P2.model) T

--24.  ��� ������� Laptop ����������� ���������� ��� �������� � 
--��� �������: code, ����� �������������� (speed, ram, hd ��� screen), �������� �������������� (CROOSS APPLY)
SELECT code, name, value FROM Laptop
CROSS APPLY (VALUES('speed =', speed), ('ram =', ram), ('hd =', hd), ('screen', screen)) char_(name, value)
ORDER BY code, name, value;