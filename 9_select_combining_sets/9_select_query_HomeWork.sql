use labor_sql 
go

--1.  �������� �������� ����� � ����� ��� .
;WITH one_cte AS
(
SELECT * 
FROM product
WHERE type = 'pc'
)
SELECT maker FROM one_cte;

--2.  �������� �������� ����� � ����� ��� (� ������ � 
--��������� �� ������) 
;WITH two_cte AS
(SELECT model,type FROM product),
thr_cte AS
(SELECT * FROM two_cte where type = 'pc' or type = 'laptop')

SELECT model from thr_cte where model like '%3%';

--3.  �������� �����  (�� �Geography�)  ������ ����� ������ 
--������� ���� 
SELECT region_id, id , name,  place_level = '1' FROM geography
WHERE region_id = '1';

--4.  �������� �����  (�� �Geography�)    ���� ����� ������ ��� 
--����������� ������ (���������,  �����-���������) 
;WITH fr_cte AS
(SELECT * , place_lvl = 0 FROM geography where region_id = '4')
select g.id,g.name,g.region_id, place_lvl + 1 as place_level  from fr_cte, geography G
where  fr_cte.id = G.region_id
union all 
SELECT * FROM fr_cte
order by region_id asc

--5.  ������� ����� ������� ������ ����������� ����� �� 1 �� 10 000.
; WITH RowCTE(row_no) AS  
(  
SELECT row_no = 1    
UNION ALL
SELECT  row_no +1  AS num  FROM RowCTE WHERE row_no < 50000
)

SELECT * FROM RowCTE
OPTION ( MAXRECURSION 50000)

--6.  ������� ����� ������� ������ ����������� ����� �� 1 �� 100 000.
;WITH [Numb] AS (
    SELECT 0 AS [Number]
    UNION ALL
    SELECT [Number] + 1
    FROM [Numb]
    WHERE [Number] < 999
)
SELECT [ts].[Number] * 1000 + [Ones].[Number]
FROM [Numb] [ts] CROSS JOIN [Numb] [Ones]
ORDER BY 1
OPTION (MAXRECURSION 1000)
GO

--7.  ��������� ������� ������ ����� � ����� � ��������� ����.
  declare @date_start datetime = '2018-01-01 00:00:00.100'
  declare @date_finsh datetime = '2018-12-31 23:59:59.100'
	    select dateadd(day, number, @date_start) as Date, DATENAME(DW, dateadd(day, number, @date_start)) as holyday into #calend_year
		from (select distinct number from master.dbo.spt_values where name is null) n
		where dateadd(day, number, @date_start) < @date_finsh 
		    select Count(holyday) as number_of_days_off 
			from #calend_year 
		    where holyday in ('Saturday', 'Sunday')
				drop table #calend_year


--8.  �� �����. �����. ������� ���������, �� ���������� 
--��, ��� �� �������� (����������� �������� IN). ������� 
--maker.
SELECT DISTINCT maker FROM Product
WHERE (maker IN (SELECT maker FROM Product WHERE type='pc') 
and maker NOT IN (SELECT maker FROM Product WHERE type ='laptop'))


--9.  �� �����. �����. ������� ���������, �� ���������� 
--��, ��� �� �������� (����������� ������� ����� ALL). 
--������� maker.
SELECT DISTINCT maker FROM Product
WHERE (maker IN (SELECT maker FROM Product WHERE type ='pc'))
AND maker <> ALL (SELECT maker FROM Product WHERE type ='laptop')


--10.  �� �����. �����. ������� ���������, �� ���������� 
--��, ��� �� �������� (����������� ������� ����� ANY). 
--������� maker.
SELECT DISTINCT maker FROM Product
WHERE (maker = ANY(SELECT maker FROM Product WHERE type='pc'))
AND maker NOT IN (SELECT maker FROM Product WHERE type ='laptop')


--11.  �� �����. �����. ������� ���������, �� ���������� 
--��������� �� �� �������� (����������� �������� IN). 
--������� maker.
SELECT DISTINCT maker FROM Product
WHERE maker IN (SELECT maker FROM Product WHERE type = 'pc')
AND type='Laptop';
 
--12.  �� �����. �����. ������� ���������, �� ���������� 
--��������� �� �� �������� (����������� ������� ����� 
--ALL). ������� maker.
SELECT DISTINCT maker FROM Product
WHERE maker  = ALL (SELECT maker FROM Product WHERE type ='pc')
AND   maker IN (SELECT maker FROM Product WHERE type ='laptop')


--13.  �� �����. �����. ������� ���������, �� ���������� 
--��������� �� �� �������� (����������� ������� ����� 
--ANY). ������� maker.
SELECT DISTINCT maker FROM Product
WHERE maker = ANY (SELECT maker FROM Product WHERE type = 'pc')
AND type='Laptop';


--14.  �� �����. �����. ������ ��� ��������� ��, �� ����� 
--�� ���� � � �������� � ������� PC (��������������� 
--�������� �������� �� ��������� IN, ALL, ANY). ������� 
--maker.
SELECT distinct p.maker 
FROM product p
LEFT JOIN pc ON pc.model = p.model
WHERE p.model = ANY  (SELECT pc.model FROM Product,pc WHERE type ='pc' AND product.model = pc.model)


--15. �� ������볻. ������� ����� ��� ������� ������ 
--('Ukraine'). ���� � �� ���� ����� ������� ������, ��� 
--������� ����� ��� ��� ������� � �� ����. �������: 
--country, class.
 SELECT CL.country, CL.class FROM classes CL
 WHERE (CL.country) = 'Ukraine' 
 AND EXISTS 
 ( SELECT CL.country, CL.class FROM classes CL WHERE (CL.country) = 'Ukraine' )
 UNION ALL SELECT CL.country, CL.class FROM classes CL WHERE NOT EXISTS 
 (SELECT CL.country, CL.class FROM classes CL WHERE (CL.country) = 'Ukraine' )

--16.  �� ������볻. ������� ������, ���������� ��� ��������� 
--����, ����� ���, �� ���� �������� � ���� � ����� ���� 
--('damaged'), � ���� (������ � ���) ����� ����� ������ � 
--������. �������: ship, battle, date.
WITH SAVES AS
(SELECT O.ship, B.name, B.date, O.result FROM OUTCOMES O
LEFT JOIN battles B ON O.battle = B.name )
SELECT DISTINCT A.ship FROM SAVES A
WHERE (A.ship) IN
(SELECT (ship) FROM SAVES B
WHERE B.date < A.date AND B.result = 'damaged')


--17.  �� �����. �����. ������ ��� ��������� ��, �� ����� 
--�� ���� � � �������� � ������� PC (�������������� 
--�������� EXISTS). ������� maker.
SELECT maker FROM product P
WHERE 
EXISTS (SELECT model FROM pc C WHERE P.model = C.model) 


--18. �� �����. �����. ������� ��������� ��������, �� 
--���������� �� � �������� �������� ���������. 
--�������: maker.
SELECT distinct product.maker
FROM product, printer, (select top 1 maker from pc,product where pc.model = product.model order by speed desc) m
Where product.model = printer.model and m.maker = product.maker and product.type = 'printer'

--19.  �� ������볻. ������� ����� �������, � ���� ���� � ���� 
--�������� ��� ���������� � ������. �������: class. (����� 
--����� ������� ��������� �� �������� Ships, ���� ���� 
--��� ����, ��� ���������� �� ���� ����� �� ������� � 
--������ �����, ����� �� � ��������) 
SELECT s.class FROM Ships s
LEFT JOIN Classes cl ON s.class = cl.class
WHERE s.class IN (SELECT ship FROM Outcomes WHERE result = 'sunk') 
OR s.name IN (SELECT ship FROM Outcomes WHERE result = 'sunk')


--20.  �� �����. �����. ������� ��������, �� ����� 
--������� ����. �������: model, price.
SELECT model, price FROM printer
WHERE price = (SELECT MAX(price) FROM printer )

--21.  �� �����. �����. ������� ��������, �������� ���� � 
--������ �� �������� ����-����� � ��. �������: type, 
--model, speed.
SELECT  DISTINCT p.type,p.model,l.speed FROM laptop l join product p on l.model=p.model
WHERE l.speed<(select min(speed) from pc)
 
--22.  �� �����. �����. ������� ��������� ����������� 
--���������� ��������. �������: maker, price.
SELECT DISTINCT product.maker, printer.price FROM product, printer
WHERE product.model = printer.model
AND printer.color = 'y'
AND printer.price = ( SELECT MIN(price) FROM printer WHERE printer.color = 'y' ) 


--23.  �� ������볻. ������ �����, � ���� ����� ������ �� 
--������� �� ��� ������� ������ � 򳺿 � ����� (���� 
--����� ���������� ����� ������� Ships, � ����� 
--������� ��� ������� Outcomes, �� ������� � ������� 
--Ships, �� ����� �� �����). �������: ����� �����, �����, 
--������� �������.
SELECT DISTINCT o.battle FROM outcomes o
right JOIN ships s ON s.name = o.ship
right JOIN classes c ON o.ship = c.class OR s.class = c.class
WHERE c.country IS NOT NULL
GROUP BY c.country, o.battle
HAVING COUNT(o.ship) >= 2


--24.  �� �����. �����. ��� ������� Product �������� 
--���������� ���� � ������ ������� � ��������� maker, 
--pc, laptop �� printer, � ��� ��� ������� ��������� 
--��������� ������� ������� ���������, �� ��� 
--�����������, ����� ������ �������� ������� ��������� � 
--��������, ��������, PC, Laptop �� Printer. 
select count(model) from product where

--25.  �� �����. �����. ��� ������� Product �������� 
--���������� ���� � ������ ������� � ��������� maker, 
--pc, � ��� ��� ������� ��������� ��������� �������, �� 
--�������� �� ('yes'), �� �� ('no') ��������� ��� 
--���������. � ������� ������� ('yes') ��������� ������� 
--����� � ������� ������ �������� ������� ������ 
--(�����, �� ����������� � ������� PC) ���������, 
--���������, 'yes(2)'. 
SELECT maker, CASE WHEN ( SELECT COUNT(*) FROM Product 
WHERE type='printer' GROUP BY maker HAVING maker=P.maker )
IS NOT NULL THEN 'yes('+str(( SELECT COUNT(*) FROM Product 
WHERE type='printer' GROUP BY maker HAVING maker=P.maker ),1)+')' ELSE 'no'
END AS Printer 
FROM Product P 
GROUP BY maker



--26.  �� �Գ��� ����. ������������. ���������, �� ������ 
--�� ������ ������ �� ������� ����� ������� ��������� 
--�� ������ ������ ���� �� ���� (���� ������� Income_o
--�� Outcome_o), �������� ����� � ������ ��������� 
--������: point (�����), date (����), inc (������), out
--(������). 
SELECT t1.point, t1.date, inc, out
FROM income_o t1 LEFT JOIN outcome_o t2 ON t1.point = t2.point
AND t1.date = t2.date
UNION
SELECT t2.point, t2.date, inc, out
FROM income_o t1 RIGHT JOIN outcome_o t2 ON t1.point = t2.point
AND t1.date = t2.date

--27.  �� ������볻. ��������� ����� ��� ������� � ������� 
--Ships, �� �������������, � ��������� �������, 
--��������� ����-���� �������� ������� � ���������� 
--������: numGuns=8, bore=15, displacement=32000, 
--type='bb', country='USA', launched=1915, class='Kongo'. 
--�������: name, numGuns, bore, displacement, type, 
--country, launched, class. 
SELECT * FROM Ships AS s JOIN Classes AS cl1 ON s.class = cl1.class
WHERE
CASE 
WHEN numGuns = '8' THEN 1 ELSE 0 END +
CASE 
WHEN bore = '15' THEN 1 ELSE 0 END +
CASE 
WHEN displacement = '32000' THEN 1 ELSE 0 END +
CASE 
WHEN type = 'bb' THEN 1 ELSE 0 END +
CASE 
WHEN launched = '1915' THEN 1 ELSE 0 END +
CASE 
WHEN s.class = 'Kongo' THEN 1 ELSE 0 END +
CASE 
WHEN country = 'USA' THEN 1 ELSE 0 END > = 3
-------------------------------���------------------------���----------------------------
select * from ships s
inner join classes c on c.class = s.class
where numGuns = 8 or bore = 15 or displacement = 32000 or
      type = 'bb' or launched = 1915 or s.class='Kongo' or country='USA'

--28.  �� �Գ��� ����. ������������. ��������� ����� �� 
--����� ������ � �������� �� ������ ����� ������ � 
--���������� �������� �� ���� ����� ������� � Outcome
--�� Outcome_o �  �� ������ ����, ���� ����������� 
--������ �������� �������� ���� � �� ������ � ���. 
--�������: ����� ������, ����, �����: � 'once a day', ���� 
--���� ������ � ������ � ����� � �������� ���� ��� �� 
--����; � 'more than once a day', ���� � � ����� � 
--�������� ������� ���� �� ����; � 'both', ���� ���� 
--������ � ���������. 
select case when o1.point is null then o2.point else o1.point end,
case when o1.date is null then o2.date else o1.date end,
case
when o1.out is null and o2.out is not null
then 'more than once a day'
when o2.out is null and o1.out is not null
then 'once a day'
when o2.out is null and o1.out is null
then 'both'
when o1.out > o2.out then 'once a day'
when o1.out < o2.out then 'more than once a day'
when o1.out = o2.out then 'both'
else 'both' end
from (select point, date, out from outcome_o
where (point in (select distinct point from outcome
intersect
select distinct point from outcome_o))) as o1 full join
(select point, left(convert(varchar, date, 121), 10) as date, sum(out) as out from outcome
where (point in (select distinct point from outcome
intersect
select distinct point from outcome_o))
group by point, left(convert(varchar, date, 121), 10)) as o2
on left(convert(varchar, o1.date, 121), 10) = o2.date and (o1.point = o2.point)


--29.  �� �����. �����. ������� ������ ������� �� ���� ��� 
--�������� (����-����� ����), �� �������� ���������� 
--'B'. �������: maker, model, type, price. 
SELECT  PP.maker, PP.model, PP.type, C.price FROM product PP JOIN pc C on PP.model = C.model
WHERE maker = 'B'
UNION ALL
SELECT P.maker, P.model, type, L.price FROM product P JOIN laptop L on P.model = L.model
WHERE maker = 'B'
UNION ALL
SELECT PD.maker, PD.model, PD.type, R.price FROM product PD JOIN printer R on PD.model = R.model
WHERE maker = 'B'
	


--30.  �� ������볻. ����������� ����� �������� �������, �� 
--� �������� � �� (��������� ����� � ������ � ������� 
--Outcomes). �������: ����� �������, class.
Select name, class from ships where class = name
union
select ship, class as name from classes,outcomes where classes.class = outcomes.ship
 
--31.  �� ������볻. ������� �����, � ���� ������� ���� ���� 
--�������� � �񳺿 �� (��������� ����� ������ � ������� 
--Outcomes, ���� ���� � ������� Ships). �������: class.
SELECT c.class FROM classes c
LEFT JOIN ( SELECT class, name  FROM ships
UNION
SELECT ship, ship  FROM outcomes) AS s ON s.class = c.class
GROUP BY c.class
HAVING COUNT(s.name) = 1

--32.  �� ������볻. ������� ����� ��� ������� � ��, ��� �� 
--����� ���������� �������, �� ���� ���� ������� �� 
--���� �� 1942 �. �������: ����� �������. 
SELECT name FROM Ships
WHERE launched < 1942