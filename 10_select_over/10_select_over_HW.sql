
--1.  �� ���������.  ������������� �� ����� � ������� Trip � 
--������� ��������� �� ������. �������� ���������� �� 
--{id_comp, trip_no}.
    SELECT row_number() over(ORDER BY id_comp, trip_no) num, trip_no, id_comp  FROM trip
    ORDER BY id_comp, trip_no;

--	2.  �� ���������.  ������������� ����� ����� �������� 
--������ � ������� ��������� ������ �����.
    SELECT row_number() over(partition BY id_comp ORDER BY id_comp,trip_no) num,  trip_no, id_comp FROM trip
    ORDER BY id_comp, trip_no;

--3.  �� �����. �����. ������� ���������� ����� �������� � 
--������ �������.
     SELECT model, color, type, price FROM 
	(SELECT *, RANK() OVER(PARTITION BY type ORDER BY price) R FROM Printer ) Ranked_models
     WHERE R = 1;

--4.  �� �����. �����.  ������ ���������, �� ���������� 
--����� 2-� ������� PC.
    SELECT maker FROM Product 
    WHERE type = 'PC'
    GROUP BY maker
    HAVING COUNT(*) > 2;

--5.  �� �����. �����.  ������ ����� �� ��������� �������� 
--���� � ������� PC.
    SELECT DISTINCT price 
	FROM(SELECT DENSE_RANK() OVER(ORDER BY price DESC) rnk, price FROM PC) X 
	WHERE rnk=2;

--6.  �� ���������.  ����������  ��������  �� 3-� ������
--������. ����� ������������ � ������� ��������� ������� 
--(����� ����� � ������� Name).
 SELECT *, NTILE(3) OVER(ORDER BY name) gr FROM passenger ORDER BY name;

--7.  �� �����. �����. ��� ������� PC ������� ����������, 
--��������� ��� ��������� �������� (�������� �� �������). 
--������� ���� ������� ���� ����������� �� ������� price �� 
--���������.  ����� ������� ������� ������ �� 3 ������.
    SELECT *,  CASE WHEN id % 3 = 0 THEN id/3 ELSE id/3 + 1 END AS page_num 
    FROM (SELECT *, ROW_NUMBER() OVER(ORDER BY price DESC) AS id, COUNT(*) OVER() AS rows_total 
    FROM pc ) X;

--8.  �� �Գ��� ����. ������������.  ������ ����������� ���� 
--�������/������  ����� ���  4-� ������� ���� �����, � ����� 
--��� ��������, ���� � ����� �������, ���� � �� ���� ���� 
--�����������.
SELECT max_sum, type, date, point FROM (SELECT MAX(inc) over() AS max_sum, * FROM 
( SELECT inc, 'inc' type, date, point FROM Income 
  UNION ALL 
  SELECT inc, 'inc' type, date, point FROM Income_o 
  UNION ALL 
  SELECT out, 'out' type, date, point FROM Outcome_o 
  UNION ALL 
  SELECT out, 'out' type, date, point FROM Outcome ) X ) Y
WHERE inc = max_sum;

--9.  �� �����. �����.  ��� ������� �� � ������� PC ������ 
--������ �� ���� ����� � ��������� ����� �� ����� � 
--����� �� ��������� �������� ��, � ����� �� ���� ����� 
--�� ��������� ��������� ����� �� �������.
    SELECT *, price - AVG(price) OVER(PARTITION BY speed) AS dprice, price - AVG (price)  OVER() as d_t_PRICE,  AVG(price) OVER( ) as TOTAL_PRICE
    FROM PC;