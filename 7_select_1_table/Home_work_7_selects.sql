--1.  �� �����. �����. ������ ��������� ��������. �������: 
--maker,  type.  ������� ���� ������������ �� ���������� �� 
--�������� maker.
	select maker,[type] from product
	where [type] = 'laptop'
	order by maker ASC
	

--2.  �� �����. �����. ������ ����� �����, �ᒺ� ����� �� 
--������ ������� ��������, ���� ���� �������� 1000 ���. 
--�������:  model,  ram,  screen,  price.  ������� ���� ������������ 
--�� ���������� �� ��������  ram  �� �� ��������� �� �������� 
--price.
	select model,ram,screen,price from laptop
	where price > 1000
	order by ram ASC , price DESC


--3.  �� �����. �����. ������� �� ������ �������  Printer  ��� 
--���������� ��������.  ������� ���� ������������ �� 
--��������� �� �������� price.
	select * from printer
	where color = 'y'
	order by price DESC
	

--4.  �� �����. �����. ������� ����� �����, �������� �� ����� 
--����� ��, �� ����� CD-������� � �������� '12�'  �� '24�'  �� 
--���� ����� 600 ���. �������:  model,  speed,  hd,  cd,  price.
--������� ���� ������������ �� ��������� �� �������� speed.
	select model,  speed,  hd,  cd,  price  from pc
	where (cd = '12x' or cd = '24x') AND price < 600
	order by speed DESC


--5.  �� ������볻. ������������ ����� �������� ������� (� 
--�������  Ships). �������:  name,  class. ������� ���� 
--������������ �� ���������� �� �������� name.
	select [name], class from ships
	order by [name] ASC


--6.  �� �����. �����.  �������� ���������� ��� ���������, �� 
--����� ������� ��������� �� ����� 500 ��� �� ���� ����� 800 
--���. ������� ���� ������������ �� ��������� �� �������� price.
	select * from pc
	where speed >= 500 and price < 800
	order by price DESC
	

--7.  �� �����. �����.  �������� ���������� ��� �� ��������, 
--�� �� � ���������� �� �������� ����� 300 ���. ������� ���� 
--������������ �� ��������� �� �������� type.
	select * from printer
	where type <> 'Matrix' AND price < 300
	order by type DESC


--8.  �� �����. �����.  ������ ������ �� ������� ��������� 
--���������, �� �������� �� 400 �� 600 ���. �������: model, 
--speed.  ������� ���� ������������ �� ���������� �� �������� 
--hd.
	select model,speed from pc
	where price between 400 and 600
	Order by hd


--9.  �� �����. �����. ������� ����� �����, �������� �� ����� 
--��������� ����� ��� ���  ��������,  ����� ���� ��    �����  12 
--�����. �������:  model,  speed,  hd,  price.  ������� ���� 
--������������ �� ��������� �� �������� price.
	select model,  speed,  hd,  price from laptop
	where screen >= 12
	order by price DESC
	

--10.  �� �����. �����. ������� ����� �����, ��� �� ���� ��� ��� 
--��������, �������  ���� �����  300 ���. �������:  model,  type, 
--price.  ������� ���� ������������ �� ��������� �� �������� 
--type.
	select model,[type],price from printer
	where price <= 300
	order by type DESC

--11.  �� �����. �����. ������� ����� �������� � ������� RAM
--����� 64 ��.  �������:  model,  ram,  price.  ������� ���� 
--������������ �� ���������� �� �������� screen. 
	select model,ram,price from laptop
	where ram = 64
	order by screen ASC


--12.  �� �����. �����.  ������� �����  ��  � �������  RAM
--������ �� 64 ��.  �������:  model,  ram,  price.  ������� ���� 
--������������ �� ���������� �� �������� hd.
	select model, ram , price from pc
	where ram > 64
	order by hd ASC


--13.  �� �����. �����. ������� ����� �� � �������� ��������� 
--� ����� �� 500 �� 750 ���.  �������:  model,  speed,  price.
--������� ���� ������������ �� ��������� �� �������� hd.
	select model,speed,price from pc	
	where speed between 500 and 750
	order by hd DESC


--14.  �� �Գ��� ����. ������������.  ������� ���������� ��� 
--������ ������ �� ���� ����� 2000 ���. �� ������� ������� 
--������� Outcome_o. ������� ���� ������������ �� ��������� ��
--�������� date.
	select * from outcome_o
	where out > 2000
	order by date DESC

--15.  �� �Գ��� ����. ������������.  ������� ���������� ��� 
--������ ������ �� ���� � ����� �� 5 ���. �� 10 ���. ���. �� 
--������� ������� ������� Income_o. ������� ���� ������������ 
--�� ���������� �� �������� inc.
	
	select * from income_o
	where inc between 5000	and 10000
	order by inc ASC


--16.  �� �Գ��� ����. ������������.  ������� ���������� ��� 
--������ ������ �� ����� ������� �1 ������� Income. ������� 
--���� ������������ �� ���������� �� �������� inc.
	select * from income
	where point = 1
	order by inc ASC

	
--17.  �� �Գ��� ����. ������������.  ������� ���������� ��� 
--������ ������ �� ����� ������� �2 �������  Outcome. 
--������� ���� ������������ �� ���������� �� �������� out.
	select * from outcome
	where point = 2
	order by out ASC


--18.  �� ������볻.  ������� ���������� ��� �� ����� ������� ��� 
--�����  'Japan'.  ������� ���� ������������ �� ��������� �� 
--�������� type.
	select * from classes
	where country = 'Japan'
	order by type DESC

--19.  �� ������볻.  ������ �� ������, �� ���� ������� �� ���� � 
--����� �� 1920 �� 1942 ������. �������:  name,  launched. 
--������� ���� ������������ �� ��������� �� �������� launched.
	select name, launched from ships
	where launched between 1920 AND 1942
	order by launched DESC


--20.  �� ������볻.  ������� �� ������, �� ����� ������ � ���� 
--'Guadalcanal'  �� �� ���� �����������.  �������:  ship,  battle, 
--result.  ������� ���� ������������ �� ���������  �� �������� 
--ship.
	select ship,  battle, result from outcomes
	where (battle ='Guadalcanal') and result <> 'sunk'  
	order by ship DESC


--21.  �� ������볻.  ������� �� ��������� ������.  �������:  ship, 
--battle,  result.  ������� ���� ������������ �� ��������� �� 
--�������� ship.
	select ship, battle, result from outcomes
	where result = 'sunk'
	order by ship DESC


--22.  �� ������볻.  ������� ����� ����� ������� � 
--��������������� �� ������, ���� 40  ���.  �������:  class, 
--displacement.  ������� ���� ������������ �� ���������� �� 
--�������� type.
	select class, displacement from classes
	where displacement >= 40000
	order by type ASC


--23.  �� ���������.  ������� ������ ��� �����, �� ������� � 
--���  'London'.  �������:  trip_no,  town_from,  town_to.  ������� 
--���� ������������ �� ���������� �� �������� time_out. 
	select trip_no,town_from, town_to from trip
	where town_from = 'London' OR town_to = 'London'
	order by time_out ASC

--	24.  �� ���������.  ������� ������ ��� �����, �� ���� ����� 
--����  'TU-134'.  �������:  trip_no,  plane,  town_from,  town_to. 
--������� ���� ������������ �� ��������� �� �������� time_out.
	select trip_no,plane ,town_from, town_to  from trip
	where plane = 'TU-134'
	order by time_out DESC


--25.  �� ���������. ������� ������ ��� �����, �� ���� �� ����� 
--���� 'IL-86'. �������: trip_no, plane, town_from, town_to. ������� 
--���� ������������ �� ���������� �� �������� plane.
	select trip_no,plane ,town_from, town_to  from trip
	where plane <> 'IL-86'
	order by plane ASC

--26.  �� ���������.  ������� ������ ��� �����, ��  ��  ������� � 
--��� 'Rostov'. �������: trip_no, town_from, town_to. ������� ���� 
--������������ �� ���������� �� �������� plane.
	select trip_no,town_from, town_to  from trip
	where town_from != 'Rostov' AND town_to !='Rostov'
	order by plane ASC


--27.  �� �����. �����.  ������� �� ����� ��, � ������� ���� � 
--���� � �� ��������.
	select * from pc
	where model like '%1_1%'


--28.  �� �Գ��� ����. ������������. � �������  Outcome  ������� 
--��� ���������� �� �������� �����.
	select * from outcome
	where date between '2001-03-01' and '2001-03-31'


--29.  �� �Գ��� ����. ������������. � ������� Outcome_o ������� 
--��� ���������� �� 14 ����� ����-����� �����.
	select * from outcome_o
	where (DATEPART(dd, date ) = 14)

--30.  �� ������볻. � �������  Ships  ������� ����� �������, �� 
--����������� �� ' W' �� ����������� ������ 'n'.
	select * from ships
	where name like 'W%n' 

--31.  �� ������볻. � �������  Ships  ������� ����� �������, �� 
--����� � ���� ���� �� ����� 'e'.
	select * from ships
	where name like '%e%e%'


--32.  �� ������볻. � �������  Ships  ������� ����� ������� �� ���� 
--�� ������ �� ����, ����� ���� �� ���������� �� ����� 'a'.
	select name , launched  from ships
	where name NOT like '%a'

--33.  �� ������볻. ������� ����� ����, �� ����������� � ���� ��� 
--�� ����� ����� �� ���������� �� ����� 'c'.
	select *  from battles
	where name like '% %' and (name NOT like '%c')

--34.  �� ���������.  � �������  Trip  ������� ���������� ��� �����, 
--�� �������� � �������� ���� �� 12 �� 17 �������� �������.
	select * from trip 
	where time_out between '12:00:00.000' and '17:00:00.000'
	

--35.  �� ���������.  � �������  Trip  ������� ���������� ��� �����, 
--�� ��������� � �������� ���� �� 17 �� 23 �������� 
--�������.
	select * from trip
	where time_in between '17:00:00.000' and '23:00:00.000'


--36.  �� ���������.  � �������  Trip  ������� ���������� ��� �����, 
--�� ��������� � �������� ���� �� 21 �� 10 �������� 
--�������.
	select * from trip
	where (time_in between '21:00:00.000' and '23:59:59.000') or (time_in between '01:00:00.000' and '09:59:59.000')  


--37.  �� ���������. � ������� Pass_in_trip ������� ����, ���� ���� 
--������ ���� � ������� ����.
	select date from pass_in_trip
	where place like '1%'
--38.  �� ���������. � ������� Pass_in_trip ������� ����, ���� ����
--������ ���� 'c' � ����-����� ���.
	select date from pass_in_trip
	where place like '%c'

--39.  �� ���������. ������� ������� �������� (����� ����� � 
--������� name), �� ����������� �� ����� '�'.
	select * from passenger
	where name like '% c%'

--40.  �� ���������. ������� ������� �������� (����� ����� � 
--������� name), �� �� ����������� �� ����� 'J'.
	select * from passenger
	where name not like '% J%'

--41.  �� �����. �����.  ������� ������� ���� �������� � 
--���������� ������� '������� ���� = '. 
	select AVG(price) as '������� ���� = '  from laptop

--42.  �� �����. �����.  ��� �������  PC  ������� ��� ���������� � 
--����������� � ������ ������, ���������,  '������: 1121', 
--'����: 600,00' � �.�.
	select * from pc

	SELECT code, '������: '+ model as model from pc --, '�������� ��� :'+ speed as cd from pc,'����.���"��� ��:'+ram as ram, '���"��� ��:'+ hd as hd, 'DVD ��������:'+ cd as cd, 'ֳ��: '+price as price  from pc
 -----????????????????????????????????????-------------



--43.  �� �Գ��� ����. ������������.  � �������  Income  ������� 
--���� � ������ ������:  ��.�����_�����.����, ���������, 
--2001.02.15 (��� ������� ����).
	--select * from income
	select convert (varchar,[date],102) from income
	 

--44.  �� ������볻.  ��� �������  Outcomes  ������� ����, � ����� 
--������� �������  result, ������� ����������� �� ������� 
--���������� �����.
	
	select ship AS '��������', battle AS '�����',
    CASE 
        WHEN result = 'sunk' THEN '����������' 
        WHEN result = 'damaged' THEN '�����������'
        ELSE '� ����'
    END
    AS '���������'  FROM outcomes

	
--45.  �� ���������.  ��� �������  Pass_in_trip  �������� ������� 
--place  ������� �� ��� ������� � �����������, ���������, 
--������ � '���: 2' �� ������ � '����: a'. 
	
	SELECT *, '��� = ' + SUBSTRING(place, 1, 1) AS '���' ,
				'���� = ' + SUBSTRING(place, 2, 2) AS '����'
	FROM pass_in_trip
  


--46.  �� ���������. ������� ����  ��� �������  Trip  � �ᒺ������� 
--���������� ���� ��������:  town_from  ��  town_to, � 
--����������� ����������� ����: 'from Rostov to Paris'.
	select trip_no,id_comp,plane,'from ' + rtrim (town_from) + ' to ' + town_to as towns, time_out, time_in
	from trip


--47.  �� ���������. ������� ��� �������  Trip  �ᒺ����� �������� 
--��� ��� ����, �� ���������� � ������ �� �������� ������� 
--(�����, ����� �) ������� ����.
	select * from trip
	--? left()

--48.  �� �����. �����.  ������� ���������, �� ���������� �� 
--������� �� �� ���� ����� ��. �������:  maker,  �����
--�������. 
	--select * from product
	select maker , count (mode)
	from product

	--where model = '%%%%'and model = '%%%%'


--49.  �� ���������.  ��� ������� ���� ���������� ������� 
--�����, �� � ����� �������� (���������, ��������).
	select  town_from , count(trip_no) as '������� ����� = ' from trip
	group by town_from

	--union
	--select  town_to, count(trip_no) from trip
	--group by town_to


--50.  �� �����. �����.  ��������� ��� ������� ����  ��������
--������� ������� �������. 
	select distinct type , count(model) as '�-�� �������' from printer
	group by type

--51.  �� �����. �����.  ���  �������  � �������  PC  ���������, 
--������ �  �����  cd  (����� � ����� ��������), � ����� ��� 
--����� �������� ������ ������ ������� �������.
	select model, cd, count(cd) as 'model = ' from pc
	group by cd, model

--52.  �� ���������. ��� ������� ����� (�������  Trip) ��������� 
--��������� ���� �������. 
	select * , DATEDIFF( time_in , time_out) as '���������'  from trip

--53.  �� �Գ��� ����. ������������.  ���  �������  Outcome  ��� 
--������� ������  ����������  ����  ������, ��  �� ����� �����, 
--��� � �� �� ����, � �����  ������� ��� ���  ���������  �� 
--����������� ����.
	select date, point,sum(out) as '����'  , min(out) as '��' ,max(out) as '����' from outcome
	group by cube (date, point)

--54.  �� ���������.  ���  �������  Pass_in_trip  ��� ������� ����� 
--(trip_no)  ����������, ������ ���� ������� ���� � ������� 
--���.
	select trip_no ,count(place) as 'occupied places ' from pass_in_trip
	GROUP BY trip_no
	

--55.  �� ���������.  ���  �������  Passenger  ����������  ������ 
--���� ��������, ������� ���� ����������� �� �����  S,  B  �� 
--�.
	select count(*) from passenger
	where (name like 'S%') or (name like 'B%') or (name like 'A%' )
