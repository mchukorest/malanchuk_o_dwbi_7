
insert into Publisher ([Publishers_Name], [Publishers_URL], created)
   VALUES('Publishing House of the Old Lion','http://www.HouseOldLion.com',GETDATE())
insert into Publisher (Publishers_Name,Publishers_URL, created)
   VALUES('Folio','http://Folio.com',GETDATE())
insert into Publisher (Publishers_Name,Publishers_URL, created)
   VALUES('A-ba-ba-ha-la-ma-ha','http://www.Ababahalamaha.com',GETDATE())
insert into Publisher (Publishers_Name,Publishers_URL, created)
   VALUES('Family Leisure Club','http://www.FLeisureClub.com',GETDATE())
insert into Publisher (Publishers_Name,Publishers_URL, created)
   VALUES('Spirit and Letter','http://www.SpiritLetter.com',GETDATE())
insert into Publisher (Publishers_Name,Publishers_URL, created)
   VALUES('Anetty Antonenko','http://www.AnettyAntonenkoPH.com',GETDATE())
insert into Publisher (Publishers_Name,Publishers_URL, created)
   VALUES('Yaroslaviv Val','http://YaroslavivVal.com',GETDATE())
insert into Publisher (Publishers_Name,Publishers_URL, created)
   VALUES('Laurus','http://www.Laurus.com',GETDATE())
insert into Publisher (Publishers_Name,Publishers_URL, created)
   VALUES('Foundations','http://www.Foundations.com',GETDATE())
insert into Publisher (Publishers_Name,Publishers_URL, created)
   VALUES('Our format','http://www.Ourformat.com',GETDATE())

insert into  Author(Name_Authors, URL_Authors) 
   VALUES('John R. R. Tolkien','www.John R. R. Tolkien.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Jane Austen','www.Jane Austen.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Philip Pullman','www.Philip Pullman.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Douglas Adams','www.Douglas Adams.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Joanne Rowling','www.Joanne Rowling.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Harper Lee','www.Harper Lee.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Alan Alexander Milne','www.Alan Alexander Milne.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('George Orwell','www.George Orwell.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Clive Staples Lewis','www.Clive Staples Lewis.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Charlotte Bronte','www.Charlotte Bronte.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Joseph Heller','www.Joseph Heller.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Emily Bronte','www.Emily Bronte.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Sebastian Folks','www.Sebastian Folks.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Daphne DuMorie','www.Daphne DuMorie.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Jerome Salinger','www.Jerome Salinger.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Kenneth Graham','www.Kenneth Graham.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Charles Dickens','www.Charles Dickens.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Louise May Olcott','www.Louise May Olcott.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Louis de Bernier','www.Louis de Bernier.com')
insert into  Author(Name_Authors, URL_Authors) 
   VALUES('Lev Tolstoy','www.Lev Tolstoy.com')




insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES ('1','ISBN2-266-11156','1','1')
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES ('2','ISBN2-266-11352','2','2')
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES ('3','ISBN2-266-11548','3','3');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES ('4','ISBN2-266-12920','4','4');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES ('5','ISBN2-266-13116','5','5');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES('6','ISBN2-266-13312','6','6');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES('7','ISBN2-266-13508','7','7');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES('8','ISBN2-266-13704','8','8');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES('9','ISBN2-266-15076','9','9');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES('10','ISBN2-266-14096','10','10');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES('11','ISBN2-266-14292','11','11');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES('12','ISBN2-266-14488','12','12');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES('13','ISBN2-266-14488','13','13');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES('14','ISBN2-266-14880','14','14');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
   VALUES('15','ISBN2-266-15076','15','15');



Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-111569','33','http://exampleboolis11156.com','130.5');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-22157','34','http://exampleboolis22157.com','112');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-33158','35','http://exampleboolis33158.com','125');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-44159','36','http://exampleboolis44159.com','138');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-55160','37','http://exampleboolis55160.com','151');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-66161','38','http://exampleboolis66161.com','164');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-77162','39','http://exampleboolis77162.com','177');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-88163','40','http://exampleboolis88163.com','190');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-99164','41','http://exampleboolis99164.com','203');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-110165','42','http://exampleboolis110165.com','216');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-121166','33','http://exampleboolis121166.com','229');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-132167','34','http://exampleboolis132167.com','242');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-143168','35','http://exampleboolis143168.com','255');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-154169','36','http://exampleboolis154169.com','268');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-165170','37','http://exampleboolis165170.com','281');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-176171','38','http://exampleboolis176171.com','294');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-187172','39','http://exampleboolis187172.com','307');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-198173','40','http://exampleboolis198173.com','320');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-209174','41','http://exampleboolis209174.com','333');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-220175','42','http://exampleboolis220175.com','346');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-231176','33','http://exampleboolis231176.com','359');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-242177','34','http://exampleboolis242177.com','372');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-253178','35','http://exampleboolis253178.com','385');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-264179','36','http://exampleboolis264179.com','398');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-275180','37','http://exampleboolis275180.com','411');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-286181','38','http://exampleboolis286181.com','424');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-297182','39','http://exampleboolis297182.com','437');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-308183','40','http://exampleboolis308183.com','450');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-319184','41','http://exampleboolis319184.com','463');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-330185','42','http://exampleboolis330185.com','476');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-341186','33','http://exampleboolis341186.com','489');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-352187','34','http://exampleboolis352187.com','502');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-363188','35','http://exampleboolis363188.com','515');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-374189','36','http://exampleboolis374189.com','528');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-385190','37','http://exampleboolis385190.com','541');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-396191','38','http://exampleboolis396191.com','554');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-407192','39','http://exampleboolis407192.com','567');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-418193','40','http://exampleboolis418193.com','580');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-429194','41','http://exampleboolis429194.com','593');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-440195','42','http://exampleboolis440195.com','606');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-451196','33','http://exampleboolis451196.com','619');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-462197','34','http://exampleboolis462197.com','632');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-473198','35','http://exampleboolis473198.com','645');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-484199','36','http://exampleboolis484199.com','658');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-495200','37','http://exampleboolis495200.com','671');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-506201','38','http://exampleboolis506201.com','684');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-517202','39','http://exampleboolis517202.com','697');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-528203','40','http://exampleboolis528203.com','710');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-539204','41','http://exampleboolis539204.com','723');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)   VALUES('ISBN2-266-550205','42','http://exampleboolis550205.com','736');

insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('43','ISBN2-266-308183','27','43');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('44','ISBN2-266-319184','28','44');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('45','ISBN2-266-330185','29','45');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('46','ISBN2-266-341186','30','46');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('47','ISBN2-266-352187','31','47');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('48','ISBN2-266-363188','32','48');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('49','ISBN2-266-374189','33','49');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('50','ISBN2-266-385190','34','50');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('16','ISBN2-266-111569','21','16');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('21','ISBN2-266-66161','26','21');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('18','ISBN2-266-33158','23','18');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('19','ISBN2-266-44159','24','19');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('20','ISBN2-266-55160','25','20');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('30','ISBN2-266-165170','35','30');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('31','ISBN2-266-176171','36','31');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('32','ISBN2-266-187172','37','32');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('33','ISBN2-266-198173','38','33');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('34','ISBN2-266-209174','39','34');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('35','ISBN2-266-220175','40','35');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No) VALUES('36','ISBN2-266-231176','41','36');

