
ALTER TABLE	Author ADD	birthday date 
ALTER TABLE	Author ADD	book_amount	int default  ('0')
ALTER TABLE	Author ADD	issue_amount int  default  ('0')
ALTER TABLE	Author ADD	total_edition int default  ('0')

ALTER TABLE Author ADD CONSTRAINT c1a CHECK (book_amount >= 0)
ALTER TABLE Author ADD CONSTRAINT c2a CHECK (issue_amount >= 0)
ALTER TABLE Author ADD CONSTRAINT c3a CHECK (total_edition >= 0)	


ALTER TABLE books ADD title text default ('Title')
ALTER TABLE books ADD edition	int default  ('1')
ALTER TABLE books ADD published	date
ALTER TABLE books ADD issue	int

ALTER TABLE books ADD CONSTRAINT c1na CHECK (edition >= 1)
		

ALTER TABLE Publisher ADD created date default  ('Jan -1 ,1900')
ALTER TABLE Publisher ADD country text default  ('USA')
ALTER TABLE Publisher ADD City text default	('NY')
ALTER TABLE Publisher ADD book_amount int default  ('0')
ALTER TABLE Publisher ADD issue_amount int default  ('0')
ALTER TABLE Publisher ADD total_edition	int default  ('0')
	

ALTER TABLE Publisher ADD CONSTRAINT c2na CHECK (book_amount >= 0)
ALTER TABLE Publisher ADD CONSTRAINT c3na CHECK (issue_amount >= 0)
ALTER TABLE Publisher ADD CONSTRAINT c4na CHECK (total_edition >= 0)	
	

ALTER TABLE Authors_log ADD	book_amount_old	int
ALTER TABLE Authors_log ADD	issue_amount_old int
ALTER TABLE Authors_log ADD	total_edition_old int
ALTER TABLE Authors_log ADD	book_amount_new	int
ALTER TABLE Authors_log ADD	issue_amount_new int
ALTER TABLE Authors_log ADD	total_edition_new int