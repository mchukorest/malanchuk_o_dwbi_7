USE [O_M_Library]
GO
/****** Object:  Trigger [dbo].[updater2]    Script Date: 25.07.2018 22:48:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER   TRIGGER [dbo].[updater2] ON [dbo].[Author]

AFTER INSERT, UPDATE, DELETE
AS BEGIN

	declare @Author_Id_new int;
	declare @Name_new nvarchar(50);
	declare @URL__new nvarchar(50);
	declare @book_amount_new int
    declare @issue_amount_new int
    declare	@total_edition_new int

	declare	@book_amount_old	int
    declare	@issue_amount_old int
    declare	@total_edition_old int
	declare @Author_Id_old int;
	declare @Name_old nvarchar(50);
	declare @URL__old nvarchar(50);
		
	declare @oper_date datetime;
	set		@oper_date = GETDATE()

					-- Get data from inserted / updated
		select @Author_Id_new = AN.Author_Id from inserted AN
		select @Name_new  = AN.Name_Authors from inserted AN
		select @URL__new = AN.URL_Authors from inserted AN
		select @book_amount_old	= AN.book_amount from inserted AN
		select @issue_amount_old = AN.issue_amount from inserted AN
		select @total_edition_old = AN.total_edition from inserted AN
	

					-- Get data from deleted
    	select @Author_Id_old = DN.Author_Id from deleted DN
		select @Name_old  = DN.Name_Authors from deleted DN
		select @URL__old = DN.URL_Authors from deleted DN
		select @book_amount_old	= DN.book_amount from deleted DN
		select @issue_amount_old = DN.issue_amount from deleted DN
		select @total_edition_old = DN.total_edition from deleted DN

					-- Insert 
		IF EXISTS( SELECT * FROM inserted) AND NOT EXISTS (SELECT * FROM deleted) 
		BEGIN
        insert into Authors_log	 (Author_Id_new, Name_new, URL_new, operation_type, operation_datetime,book_amount_new,issue_amount_new,total_edition_new)
		Values (@Author_Id_new,@Name_new,@URL__new,'I',@oper_date,@book_amount_new,@issue_amount_new,@total_edition_new);
		END
    
					-- Update 
		IF EXISTS( SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted)
		BEGIN
		INSERT INTO  Authors_log (Author_Id_new,Name_new, URL_new, Author_Id_old,Name_old,URL_old,operation_type,operation_datetime,book_amount_new,issue_amount_new,total_edition_new,book_amount_old,issue_amount_old,total_edition_old)
		values (@Author_Id_new,@Name_new,@URL__new,@Author_Id_old,@Name_old,@URL__old,'U',@oper_date,@book_amount_new,@issue_amount_new,@total_edition_new,@book_amount_old,@issue_amount_old,@total_edition_old);
		END
      
				    -- Delete 
        IF EXISTS( SELECT * FROM deleted) AND NOT EXISTS (SELECT * FROM inserted)
        BEGIN      
        insert into Authors_log	 (Author_Id_new, Name_new, URL_new, operation_type, operation_datetime,book_amount_old,issue_amount_old,total_edition_old)
		Values (@Author_Id_old,@Name_old,@URL__old,'D',@oper_date,@book_amount_old,@issue_amount_old,@total_edition_old);

END
END
