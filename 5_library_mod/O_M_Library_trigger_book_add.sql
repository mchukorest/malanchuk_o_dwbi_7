
--2.2) �������� ����� ��� �������� Books, ������ ��� ����������� ����� ��� ����� �������� � �������
--Publisher �������: book_amount, issue_amount, total_edition
CREATE OR ALTER TRIGGER trigg_insert
   ON  [dbo].[Books]
   AFTER INSERT,UPDATE
AS 
BEGIN

	declare @publish_id_num int 
	set @publish_id_num = (select Publisher_Id from inserted)

		IF EXISTS( SELECT * FROM inserted) AND NOT EXISTS (SELECT * FROM deleted)
		BEGIN
		Update Publisher
		set book_amount = book_amount + 1 , issue_amount = issue_amount +1 ,total_edition = total_edition + 1
		where Publisher_Id = @publish_id_num
		END
END 