use people_ua_db
go

SET NOCOUNT ON; 
DECLARE @object_id int; 
DECLARE @index_id int;
DECLARE @part_count bigint; 
DECLARE @schem_name nvarchar(130); 
DECLARE @obj_name nvarchar(130); 
DECLARE @index_name nvarchar(130); 
DECLARE @part_num bigint; 
DECLARE @frag float;
DECLARE @cmd nvarchar(4000); 

SELECT
    object_id AS objectid,
    index_id AS indexid,
    partition_number AS partitionnum,
    avg_fragmentation_in_percent AS frag
INTO #temp_work
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, 'LIMITED')
WHERE avg_fragmentation_in_percent > 50.0 AND index_id > 0 AND page_count > 128;

DECLARE partitions CURSOR FOR SELECT * FROM #temp_work;

OPEN partitions;

WHILE (1=1)
    BEGIN;
        FETCH NEXT
           FROM partitions
           INTO @object_id, @index_id, @part_num, @frag;
        IF @@FETCH_STATUS < 0 BREAK;
		
        SELECT @obj_name = QUOTENAME(o.name), @schem_name = QUOTENAME(s.name)
        FROM sys.objects AS o
        JOIN sys.schemas as s ON s.schema_id = o.schema_id
        WHERE o.object_id = @object_id;
        SELECT @index_name = QUOTENAME(name)
        FROM sys.indexes
        WHERE  object_id = @object_id AND index_id = @index_id;
        SELECT @part_count = count (*)
        FROM sys.partitions
        WHERE object_id = @object_id AND index_id = @index_id;

        IF @frag <= 80.0
            SET @cmd = N'ALTER INDEX ' + @index_name + N' ON ' + @schem_name + N'.' + @obj_name + N' REORGANIZE';
        IF @frag > 80.0
            SET @cmd = N'ALTER INDEX ' + @index_name + N' ON ' + @schem_name + N'.' + @obj_name + N' REBUILD';
        IF @part_count > 1
            SET @cmd = @cmd + N' PARTITION=' + CAST(@part_num AS nvarchar(10));

    END;

CLOSE partitions;
DEALLOCATE partitions;

 select * from #temp_work
 Go
