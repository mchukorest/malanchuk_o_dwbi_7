use O_M_module_3 
go

insert into storage (product_name, product_factory, product_type, product_amount,amount_unit,supply_by)
values('Win 7','USA','OS',24,'pcs','POST');

insert into storage (product_name,product_factory,product_type,product_amount,amount_unit,supply_by)
values ('Win 8.1','USA','OS',15,'pcs','POST');

insert into storage (product_name,product_factory,product_type,product_amount,amount_unit,supply_by)
values ('Win 10','USA','OS',19,'pcs','POST');

insert into storage (product_name,product_factory,product_type,product_amount,amount_unit,supply_by)
values ('Win XP','USA','OS',10,'pcs','POST');

insert into storage (product_name,product_factory,product_type,product_amount,amount_unit,supply_by)
values ('Win bla bla ','USA','OS',10,'bla bla bal ','POST');