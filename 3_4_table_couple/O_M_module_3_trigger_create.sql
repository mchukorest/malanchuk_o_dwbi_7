use O_M_module_3
go

Create or ALTER TRIGGER updater_storage ON [dbo].[storage]

AFTER INSERT, UPDATE, DELETE
AS BEGIN

	declare @storage_id_new		 int;
	declare @product_name_new	 nvarchar(50);
	declare @product_factory_new nvarchar(50);
	declare @product_type_new	 nvarchar(50);
	declare @product_amount_new	 int;
	declare @amount_unit_new	 nchar (3) ;

	declare @storage_id_old		 int;
	declare @product_name_old	 nvarchar(50);
	declare @product_factory_old nvarchar(50);
	declare @product_type_old	 nvarchar(50);
	declare @product_amount_old	 int;
	declare @amount_unit_old	 nchar (3) ;

	declare @oper_date datetime;
	set		@oper_date = GETDATE()

					-- Get data from supply_date / updated
		select @storage_id_new = ET.storage_id			 from inserted ET
		select @product_name_new  = ET.product_name		 from inserted ET
		select @product_factory_new = ET.product_factory from inserted ET
		select @product_type_new = ET.product_type		 from inserted ET
		select @product_amount_new = ET.product_amount	 from inserted ET
		select @amount_unit_new	= ET.amount_unit		 from inserted ET


					-- Get data from deleted
    	select @storage_id_old = DE_T.storage_id			 from deleted DE_T
		select @product_name_old = DE_T.product_name		 from deleted DE_T
		select @product_factory_old = DE_T.product_factory	 from deleted DE_T
		select @product_type_old = DE_T.product_type		 from deleted DE_T
		select @product_amount_old = DE_T.product_amount	 from deleted DE_T
		select @amount_unit_old	= DE_T.amount_unit			 from deleted DE_T

					-- Insert 
		IF EXISTS( SELECT * FROM inserted) AND  NOT EXISTS (SELECT * FROM deleted) 
		BEGIN
        insert into storage_log	 (storage_id_new, product_name_new, product_factory_new, product_type_new, product_amount_new,amount_unit_new, operation_type, operation_datetime)
					Values (@storage_id_new,@product_name_new,@product_factory_new,@product_type_new,@product_amount_new, @amount_unit_new,'I',@oper_date);
		END
    
					-- Update 
		IF EXISTS( SELECT * FROM inserted) AND  EXISTS (SELECT * FROM deleted)
		BEGIN
		 insert into storage_log	 (storage_id_new, product_name_new, product_factory_new, product_type_new, product_amount_new,amount_unit_new,storage_id_old, product_name_old, product_factory_old, product_type_old, product_amount_old,amount_unit_old, operation_type, operation_datetime)
							Values (@storage_id_new,@product_name_new,@product_factory_new,@product_type_new,@product_amount_new, @amount_unit_new,@storage_id_old,@product_name_old,@product_factory_old,@product_type_old,@product_amount_old, @amount_unit_old,'U',@oper_date);
			END
      
				    -- Delete 
        IF EXISTS( SELECT * FROM deleted) AND NOT EXISTS (SELECT * FROM inserted)
        BEGIN      
		insert into storage_log	 (storage_id_new, product_name_new, product_factory_new, product_type_new, product_amount_new,amount_unit_new, operation_type, operation_datetime)
					Values (@storage_id_old,@product_name_old,@product_factory_old,@product_type_old,@product_amount_old, @amount_unit_old,'D',@oper_date);
		
END
END

