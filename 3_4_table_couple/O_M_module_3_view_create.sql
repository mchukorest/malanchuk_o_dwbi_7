use O_M_module_3
go

Create view Client_view
AS SELECT * FROM O_M_module_3.dbo.client
GO

Create view Product_view
AS SELECT * FROM O_M_module_3.dbo.product
GO


Create view Order_view
AS SELECT * FROM O_M_module_3.dbo.[order]

go
GO

-- with CHECK OPTION

  Create view Client_view_ck
AS SELECT * FROM O_M_module_3.dbo.client
WHERE country_client = 'Canada'
WITH CHECK OPTION
GO

Create view Product_view_ck
AS SELECT * FROM O_M_module_3.dbo.product
WHERE standart_price  >= 100
WITH CHECK OPTION
GO


Create view Order_view_ck
AS SELECT * FROM O_M_module_3.dbo.[order]
WHERE status_order  = 'recived'
WITH CHECK OPTION
go
GO
