
-- if exists

DROP TABLE  if exists [dbo].[order];
GO
DROP TABLE  if exists [dbo].[product];
GO
DROP TABLE  if exists [dbo].[client];
GO
DROP DATABASE if exists O_M_module_3;
GO


-- start 1 execute

CREATE DATABASE O_M_module_3;
go
-- start 2 execute

USE O_M_module_3;
go

-- start 3 execute
--************************************** [dbo].[product]

CREATE TABLE [dbo].[product]
(
 [id_product]       INT IDENTITY (1, 1) NOT NULL ,
 [bar_code_product] BIGINT  NOT NULL,
 [name_product]     NVARCHAR(50) NOT NULL ,
 [standart_price]   DECIMAL(8,2) NOT NULL ,
 [size_product]     NVARCHAR(50) NOT NULL ,
 [weight_product]   DECIMAL(8,2) NOT NULL ,
 [weight_unit]      NCHAR(3) NOT NULL ,
 [made_date]        DATE NOT NULL ,
 [color_product]    NVARCHAR(50) NOT NULL ,
 [type_product]     NVARCHAR(50) NOT NULL ,

 CONSTRAINT limitation_1 CHECK (standart_price >= 1),
 CONSTRAINT limitation_3 CHECK (weight_unit in ('g','kg','ft')),

 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED ([id_product] ASC, [bar_code_product] ASC)
);
GO

--************************************** [dbo].[client]
CREATE TABLE [dbo].[client]
(
 [id_client]  INT IDENTITY (1001, 1) NOT NULL ,
 [first_name_client] NVARCHAR(50) NOT NULL ,
 [last_name_client]  NVARCHAR(50) NULL ,
 [phone_1_client]    NVARCHAR(50) NOT NULL ,
 [date_birth]		 DATE NOT NULL ,
 [country_client]    NVARCHAR(50) NOT NULL ,
 [city_client]		 NVARCHAR(50) NOT NULL ,
 [address_client]    NVARCHAR(50) NOT NULL ,
 [email_client]      NVARCHAR(50) NOT NULL ,
 [company_client]    NVARCHAR(50) NULL ,
 [registration_date] DATETIME NOT NULL default GETDATE(),

  CONSTRAINT [PK_client] PRIMARY KEY CLUSTERED ([id_client] ASC)

);
GO
--************************************** [dbo].[order]

CREATE TABLE [dbo].[order]
(
 [id_order]         INT IDENTITY (1, 1) NOT NULL ,
 [invoice_id]       BIGINT  NOT NULL ,
 [id_product]       INT NOT NULL ,
 [bar_code_product] BIGINT NOT NULL ,
 [product_price]    DECIMAL(8,2) NOT NULL ,
 [product_amount]   INT NOT NULL ,
 [total_price]      AS ([product_price]  *  [product_amount]),--  DECIMAL(16,2) NOT NULL ,
 [order_date]       DATETIME NOT NULL default GETDATE(),
 [id_client]		INT  NOT NULL ,
 [status_order]     NVARCHAR(50) NOT NULL ,
 [date_dispatch]    DATE NOT NULL,
 [date_refrsh]      DATETIME NULL,

 
  CONSTRAINT limitation_2 CHECK (product_price >= 1 AND status_order in ('processing','sent','recived')),
  CONSTRAINT [PK_order] PRIMARY KEY CLUSTERED ([id_order] ASC),

);

GO

GO

--  FK  separately


ALTER TABLE  [dbo].[order]    
ADD CONSTRAINT [FK_relationship_22] FOREIGN KEY ([id_product], [bar_code_product] )
	REFERENCES [dbo].[product]([id_product], [bar_code_product]) ON DELETE CASCADE
	-- ON UPDATE CASCADE ;
GO
  
ALTER TABLE  [dbo].[order]
ADD  CONSTRAINT [FK_relationship_21] FOREIGN KEY ([id_client])
  REFERENCES [dbo].[client] ([id_client]) ON DELETE CASCADE 
  --ON UPDATE CASCADE ;    
GO    

-- start 4 execute

-- insert data into product

INSERT INTO dbo.product ( bar_code_product, name_product, standart_price, size_product, weight_product, weight_unit, made_date, color_product, type_product)
VALUES ( 10001,'ware one', 50, '10x10x50', 2.5 , 'kg' , '2008-12-10' , 'light red', 'vase');

INSERT INTO dbo.product ( bar_code_product, name_product, standart_price, size_product, weight_product, weight_unit, made_date, color_product, type_product)
VALUES ( 10002,'ware one-p-one', 45, '10x10x30', 1.5 , 'kg' , '2017-02-10' , 'light black', 'vase');

INSERT INTO dbo.product ( bar_code_product, name_product, standart_price, size_product, weight_product, weight_unit, made_date, color_product, type_product)
VALUES ( 10003,'ware two', 1050, '50x100x180', 45.5 , 'kg' , '2017-03-10' , 'shadow', 'statuet');

INSERT INTO dbo.product ( bar_code_product, name_product, standart_price, size_product, weight_product, weight_unit, made_date, color_product, type_product)
VALUES ( 10004,'ware five', 500, '100x30x50', 12.5 , 'kg' , '2017-04-11' , 'light red', 'predmet');

INSERT INTO dbo.product ( bar_code_product, name_product, standart_price, size_product, weight_product, weight_unit, made_date, color_product, type_product)
VALUES ( 10005,'ware ten', 2500, '100x110x95', 20 , 'kg' , '2017-07-02' , 'black', 'vase');

INSERT INTO dbo.product ( bar_code_product, name_product, standart_price, size_product, weight_product, weight_unit, made_date, color_product, type_product)
VALUES ( 10006,'ware three', 50, '20x15x50', 18 , 'kg' , '2016-12-10' , 'light yellow', 'vase');

INSERT INTO dbo.product ( bar_code_product, name_product, standart_price, size_product, weight_product, weight_unit, made_date, color_product, type_product)
VALUES ( 10007,'ware soll', 50, '10x10x50', 2.5 , 'kg' , '2010-09-22' , 'dark brown', 'vase');

-- insert data into clent

INSERT INTO dbo.client (first_name_client,last_name_client,phone_1_client,date_birth,country_client,city_client,address_client,email_client,company_client)
VALUES ('John','Swipe','+310 555 0105','1972-01-20','USA','New York','5h Avenu','johnSw@mail.net','IBM');

INSERT INTO dbo.client (first_name_client,last_name_client,phone_1_client,date_birth,country_client,city_client,address_client,email_client,company_client)
VALUES ('Michel','Newan','+310 555 0255','1993-08-02','Canada','Toronto','st. Marge 22','newmike@mail.net','MSVC');

INSERT INTO dbo.client (first_name_client,last_name_client,phone_1_client,date_birth,country_client,city_client,address_client,email_client,company_client)
VALUES ('Dom','Black','+310 555 0500','1985-05-25','Canada','Toronto','st. Colams 21','dombl2@mail.net','MSVC');

-- insert data into order

INSERT INTO dbo.[order] (invoice_id,id_product,bar_code_product,product_price,product_amount,id_client,status_order,date_dispatch)
VALUES (101,2,10002,60,2,1001,'recived','2018-07-18');

INSERT INTO dbo.[order] (invoice_id,id_product,bar_code_product,product_price,product_amount,id_client,status_order,date_dispatch)
VALUES (102,3,10003,1200,2,1001,'sent','2018-07-19');

INSERT INTO dbo.[order] (invoice_id,id_product,bar_code_product,product_price,product_amount,id_client,status_order,date_dispatch)
VALUES (103,2,10002,55,1,1003,'processing','2018-08-20');

INSERT INTO dbo.[order] (invoice_id,id_product,bar_code_product,product_price,product_amount,id_client,status_order,date_dispatch)
VALUES (103,4,10004,600,2,1003,'processing','2018-08-20');

INSERT INTO dbo.[order] (invoice_id,id_product,bar_code_product,product_price,product_amount,id_client,status_order,date_dispatch)
VALUES (103,6,10006,60,5,1003,'processing','2018-08-20');


-- select test 

select * from dbo.[client];
select * from dbo.[product];
select * from dbo.[order];