CREATE TABLE storage 
(
	storage_id		int  IDENTITY(1, 1) NOT NULL ,
	product_name	nvarchar(50) NOT NULL,
	product_factory	nvarchar(50) NOT NULL default 'Microsoft',
	product_type	nvarchar(50) null,
	product_amount	int NOT NULL,
	amount_unit		nchar (3) NOT NULL, 
	supply_date		datetime NOT NULL default  getdate(),
	supply_by		nvarchar(50) NULL,
	updated			datetime  NULL default  getdate(),
	updated_by		nvarchar(50)  NULL,


	CONSTRAINT check1 CHECK (amount_unit in ('pcs','kg')),

	CONSTRAINT [PK_Author] PRIMARY KEY NONCLUSTERED ([storage_id] ASC)
);

CREATE TABLE storage_log	
(		
	operation_id		int IDENTITY (1, 1) NOT NULL,
	storage_id_new		int  NULL,
	product_name_new	nvarchar(50)  NULL,
	product_factory_new	nvarchar(50)  NULL,
	product_type_new	nvarchar(50) null,
	product_amount_new	int NOT NULL,
	amount_unit_new		nchar (3) NOT NULL,
	storage_id_old		int  NULL,
	product_name_old	nvarchar(50)  NULL,
	product_factory_old	nvarchar(50)  NULL,
	product_type_old	nvarchar(50) null,
	product_amount_old	int  NULL,
	amount_unit_old		nchar (3)  NULL,
	operation_type		nvarchar(50) NOT NULL,
	operation_datetime	datetime NOT NULL DEFAULT getdate(),
	

	CONSTRAINT [PK_storage_log] PRIMARY KEY NONCLUSTERED ([operation_id] ASC),
	CONSTRAINT check2 CHECK (operation_type in ('I','U','D')),
);





