CREATE DATABASE education
go

USE [education]
GO
CREATE SCHEMA [O_Malanchuk]
GO

CREATE SYNONYM [O_Malanchuk].Client_Synonym
FOR O_M_module_3.[dbo].[client]
GO

CREATE SYNONYM [O_Malanchuk].storage_log_Synonym
FOR O_M_module_3.[dbo].[storage_log]
GO
CREATE SYNONYM [O_Malanchuk].storage_Synonym
FOR O_M_module_3.[dbo].[storage]
GO
CREATE SYNONYM [O_Malanchuk].product_Synonym
FOR O_M_module_3.[dbo].[product]
GO
CREATE SYNONYM [O_Malanchuk].order_Synonym
FOR O_M_module_3.[dbo].[order]
GO



select * from [O_Malanchuk].product_Synonym;
select * from [O_Malanchuk].Client_Synonym;
select * from [O_Malanchuk].storage_Synonym;
select * from [O_Malanchuk].order_Synonym;
