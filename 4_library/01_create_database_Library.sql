drop  database  if exists O_M_Library
go

CREATE DATABASE O_M_Library
go

USE O_M_Library
go

-- Create a new file group
ALTER DATABASE O_M_Library 
   ADD FILEGROUP [DATA]
go

-- Add a file to the file group, we can now use the file group to store data

ALTER DATABASE O_M_Library
   ADD FILE (
      NAME = [DATA],
      FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER_OM\MSSQL\DATA\O_M_Library.ndf',
      SIZE = 5MB,
      MAXSIZE = 100MB,
      FILEGROWTH = 5MB
      )
   TO FILEGROUP [DATA]

go

-- Change the default filegroup for where new tables and indexes are created.
ALTER DATABASE O_M_Library
   MODIFY FILEGROUP [DATA] DEFAULT;

GO