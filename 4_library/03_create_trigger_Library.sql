drop trigger if exists updater2
go

drop trigger if exists error_delete
go

use O_M_Library
go


Create or ALTER TRIGGER [dbo].[updater2] ON [dbo].[Author]

AFTER INSERT, UPDATE, DELETE
AS BEGIN

	declare @Author_Id_new int;
	declare @Name_new nvarchar(50);
	declare @URL__new nvarchar(50);

	declare @Author_Id_old int;
	declare @Name_old nvarchar(50);
	declare @URL__old nvarchar(50);
	
	declare @oper_date datetime;
	set		@oper_date = GETDATE()

					-- Get data from inserted / updated
		select @Author_Id_new = AN.Author_Id from inserted AN
		select @Name_new  = AN.Name_Authors from inserted AN
		select @URL__new = AN.URL_Authors from inserted AN

					-- Get data from deleted
    		select @Author_Id_old = DN.Author_Id from deleted DN
		select @Name_old  = DN.Name_Authors from deleted DN
		select @URL__old = DN.URL_Authors from deleted DN

					-- Insert 
		IF EXISTS( SELECT * FROM inserted) AND NOT EXISTS (SELECT * FROM deleted) 
		BEGIN
        insert into Authors_log	 (Author_Id_new, Name_new, URL_new, operation_type, operation_datetime)
		Values (@Author_Id_new,@Name_new,@URL__new,'I',@oper_date);
		END
    
					-- Update 
		IF EXISTS( SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted)
		BEGIN
		INSERT INTO  Authors_log (Author_Id_new,Name_new, URL_new, Author_Id_old,Name_old,URL_old,operation_type,operation_datetime)
		values (@Author_Id_new,@Name_new,@URL__new,@Author_Id_old,@Name_old,@URL__old,'U',@oper_date);
		END
      
				    -- Delete 
        IF EXISTS( SELECT * FROM deleted) AND NOT EXISTS (SELECT * FROM inserted)
        BEGIN      
        insert into Authors_log	 (Author_Id_new, Name_new, URL_new, operation_type, operation_datetime)
		Values (@Author_Id_old,@Name_old,@URL__old,'D',@oper_date);
END
END


create or alter trigger error_delete ON Authors_log
instead of 	delete as 	set nocount on
 declare @ERROR_MESSAGE varchar(400)
 select	@ERROR_MESSAGE		= 
 	'Trigger TR_PREVENT_DELETE_FROM_Authors_log - '+
	'Delete not allowed on table Authors_log'
raiserror( @ERROR_MESSAGE, 16, 1 )
if @@trancount > 0 begin rollback end
return
go