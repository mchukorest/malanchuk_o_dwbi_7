
use O_M_Library
go

drop table if exists [dbo].[Author]
go
drop table if exists [dbo].[Books]
go
drop table if exists [dbo].[BooksAuthors]
go
drop table if exists [dbo].[Author_log]
go
drop table if exists [dbo].[Publisher]
go

CREATE SEQUENCE s1
    START WITH 1
    INCREMENT BY 1
    NO CACHE
    ;
GO

CREATE SEQUENCE s2
    START WITH 1
    INCREMENT BY 1
    NO CACHE
    ;
GO

CREATE TABLE Author 
(
	Author_Id		int   NOT NULL DEFAULT (NEXT VALUE FOR dbo.s1),
	Name_Authors	nvarchar(50) NOT NULL UNIQUE,
	URL_Authors		nvarchar(50) NOT NULL default 'www.author_name.com', 
	inserted		datetime NOT NULL default  getdate(),
	inserted_by		nvarchar(50) NOT NULL default  system_user,
	updated			datetime  NULL,
	updated_by		nvarchar(50)  NULL,



	CONSTRAINT [PK_Author] PRIMARY KEY NONCLUSTERED ([Author_Id] ASC)
);


CREATE TABLE Books 
(
	ISBN			nvarchar(50) NOT NULL,
	Publisher_Id	int NOT NULL,
	URL_Books		nvarchar(50) NOT NULL UNIQUE,
	Price			decimal(8, 2) NOT NULL default 0,
	inserted		datetime NOT NULL default  getdate(),
	inserted_by		nvarchar(50) NOT NULL default  system_user,
	updated			datetime  NULL,
	updated_by		nvarchar(50) NULL,

	CONSTRAINT c3n CHECK (Price >= 0),
	CONSTRAINT [PK_Books] PRIMARY KEY NONCLUSTERED ([ISBN] ASC)
);
		
CREATE TABLE BooksAuthors 
(	
	BooksAuthors_id	int NOT NULL default 1,
	ISBN			nvarchar(50) NOT NULL,
	Author_Id		int NOT NULL UNIQUE,
	Seq_No			int NOT NULL default 1,
	inserted		datetime NOT NULL default  getdate(),
	inserted_by		nvarchar(50) NOT NULL default  system_user,	
	updated			date NULL,
	updated_by		nvarchar(50)  NULL,
	
	CONSTRAINT c1n CHECK (BooksAuthors_id >= 1),
	CONSTRAINT c2n CHECK (Seq_No >= 1),
	CONSTRAINT [PK_BooksAuthors] PRIMARY KEY NONCLUSTERED ([BooksAuthors_id] ASC)
);

CREATE TABLE Publisher 
(		
	Publisher_Id	int   NOT NULL default (NEXT VALUE FOR dbo.s2),
	Publishers_Name	nvarchar(50) NOT NULL UNIQUE,
	Publishers_URL	nvarchar(50) NOT NULL default 'www.publisher_name.com', 
	inserted		datetime NOT NULL default  getdate(),
	inserted_by		nvarchar(50) NOT NULL default  system_user,	
	updated			date NULL,
	updated_by		nvarchar(50) NULL,

	CONSTRAINT [PK_Publisher] PRIMARY KEY NONCLUSTERED ([Publisher_Id] ASC)

);

CREATE TABLE Authors_log	
(		
	operation_id		int IDENTITY (1, 1) NOT NULL,
	Author_Id_new		int  NULL,
	Name_new			nvarchar(50)  NULL,
	URL_new				nvarchar(50)  NULL,
	Author_Id_old		int  NULL,
	Name_old			nvarchar(50)  NULL,
	URL_old				nvarchar(50)  NULL,
	operation_type		nvarchar(50) NOT NULL,
	operation_datetime	datetime NOT NULL DEFAULT getdate(),
	

	CONSTRAINT [PK_Authors_log] PRIMARY KEY NONCLUSTERED ([operation_id] ASC),
	CONSTRAINT c4n CHECK (operation_type in ('I','U','D')),
);



ALTER TABLE  [dbo].[Books]    
ADD CONSTRAINT [FK_r_hp_1] FOREIGN KEY ([publisher_id] )
	REFERENCES [dbo].[Publisher]([publisher_id]) ON DELETE CASCADE; 
GO

ALTER TABLE  [dbo].[BooksAuthors]    
ADD CONSTRAINT [FK_r_hp_2] FOREIGN KEY ([ISBN] )
	REFERENCES [dbo].[Books]([ISBN]) ON DELETE CASCADE; 
GO

ALTER TABLE  [dbo].[BooksAuthors]    
ADD CONSTRAINT [FK_r_hp_3] FOREIGN KEY ([author_id] )
	REFERENCES [dbo].[Author]([author_id]) ON DELETE CASCADE; 
GO


select * from Author;
select * from Books;
select * from Publisher;
select * from BooksAuthors;