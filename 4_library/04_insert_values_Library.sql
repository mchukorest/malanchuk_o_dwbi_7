use O_M_Library
go
	--insert into  Publisher

insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Pearson','http://www.wmeadsiad.com');
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Reed Elsevier','http://megme.medu.com'); 
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('ThomsonReuters','http://www.mecsn.medu.com'); 
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Wolters Kluwer','http://www.law.medu.com'); 
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Random House','http://www.ict.medu.com'); 
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Hachette Livre','http://www.monrad.com'); 
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Grupo Planeta','http://minmedu.karmelia.com'); 
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('McGraw-Hill Education','http://www.stavminoadr.com'); 
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Holtzbrinck','http://www.minomos.com'); 
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Scholastic (corp.)','http://www.minoadraz.com');
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Cengage','http://www.vrnmedu.com');
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Wiley','http://www.medunso.com');
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('De Agostini Editore','http://medu.tomsk.gov.com');
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Shueisha','http://www.meducom.com');
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Kodansha','http://www.avorontcov.com');
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Springer Science and Business Media','http://adogoslovime.pro/');
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Houghton Mifflin Harcourt','http://www.vlgrmegmedu.com');
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Shogakukan','http://www.meducaltai.com');
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Harper Collins','http://www.kaka.gov.com');
insert into Publisher (Publishers_Name,Publishers_URL)
VALUES('Informa','http://www.adaikal.com'); 



			--insert into Author
Insert into Author(Name_Authors, URL_Authors)
VALUES('Samoylov Roman','http://www.websib.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Knyazhnin Bratislav','http://ege.edu.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Verigin Artemidor','http://www.ecsn.edu.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Voevodskiy Kim','http://www.law.edu.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Krauze Gavriil','http://www.ict.edu.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Vorot Leonid','http://www.monrb.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Kalinov Asteriy','http://minedu.karelia.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Troykin Zinon','http://www.stavminobr.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Kostrov Anfim','http://www.minomos.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Meshherskiy Benedikt','http://www.minobraz.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Shihmatov Varnava','http://www.vrnedu.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Menyaev Tit','http://www.edunso.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Simolin Andrian','http://edu.tomsk.gov.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Sablukov Eremey','http://www.educom.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Vasyutinskiy Gennadiy','http://www.avorontcov.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Karno Eve','http://bogoslovie.pro/');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Matke Ryurik','http://www.vlgregedu.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Yukov Kallistrat','http://www.educaltai.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Mendeleev Aleksey','http://www.kaka.gov.com');
Insert into Author(Name_Authors, URL_Authors)
VALUES('Cherepov Amos','http://www.baikal.com');



			--insert into  Books
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-11156','1','http://example.com/','120.5');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-11352','2','http://argument.example.com/','150.5');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-11548','2','https://example.com/base.php','120.5');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-11744','3','https://argument.example.com/afterthought.aspx','150.5');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-11940','3','http://example.net/bite','70');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-12136','3','https://example.com/','40');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-12332','3','http://example.com/behavior','25');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-12528','4','http://www.example.com/book/adjustment.aspx','33');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-12724','4','http://www.example.net/bird','80');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-12920','4','http://example.com/?brass=arch&bedroom=apparatus','95');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-13116','4','http://www.example.net/authority/bell','14');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-13312','5','https://www.example.com/brick.aspx','12');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-13508','5','https://example.com/#balance','87');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-13704','6','http://www.example.net/boundary.php','100');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-14096','8','https://example.com/afternoon/birth.php?baby=box','95');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-14292','9','https://arm.example.com/account/badge','40');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-14488','10','http://bird.example.org/','64');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-14684','11','https://example22.com/','88');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-14880','12','http://www.example.org/','112');
Insert into Books (ISBN,Publisher_Id,URL_Books,Price)
VALUES('ISBN2-266-15076','13','http://www.example22.org/','136');



			--insert into BooksAuthors

insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
values ('1','ISBN2-266-11156','1','1')
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
values ('2','ISBN2-266-11352','2','2')
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES ('3','ISBN2-266-11548','3','3');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES ('4','ISBN2-266-12920','4','4');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES ('5','ISBN2-266-13116','5','5');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES('6','ISBN2-266-13312','6','6');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES('7','ISBN2-266-13508','7','7');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES('8','ISBN2-266-13704','8','8');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES('9','ISBN2-266-15076','9','9');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES('10','ISBN2-266-14096','10','10');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES('11','ISBN2-266-14292','11','11');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES('12','ISBN2-266-14488','12','12');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES('13','ISBN2-266-14488','13','13');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES('14','ISBN2-266-14880','14','14');
insert into BooksAuthors (BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES('15','ISBN2-266-15076','15','15');
