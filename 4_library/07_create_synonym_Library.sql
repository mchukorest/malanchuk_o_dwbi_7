CREATE DATABASE O_M_Library_Synonym
go

USE O_M_Library_Synonym
go

CREATE SYNONYM  Author_Synonym
FOR O_M_Library.dbo.Author
GO
CREATE SYNONYM  Authors_log_Synonym
FOR O_M_Library.dbo.Authors_log
GO
CREATE SYNONYM  Books_Synonym
FOR O_M_Library.dbo.Books
GO
CREATE SYNONYM  BooksAuthors_Synonym
FOR O_M_Library.dbo.BooksAuthors
GO
CREATE SYNONYM  Publisher_Synonym
FOR O_M_Library.dbo.Publisher
GO
