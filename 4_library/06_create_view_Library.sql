CREATE DATABASE O_M_Library_view
go

USE O_M_Library_view
go

CREATE VIEW [dbo].[Author_View]
AS SELECT * FROM O_M_Library.dbo.Author
GO
CREATE VIEW [dbo].[Authors_log_View]
AS SELECT * FROM O_M_Library.dbo.Authors_log
GO
CREATE VIEW [dbo].[Books_View]
AS SELECT * FROM O_M_Library.dbo.Books
GO
CREATE VIEW [dbo].[BooksAuthors_View]
AS SELECT * FROM O_M_Library.dbo.BooksAuthors
GO
CREATE VIEW [dbo].[Publisher_View]
AS SELECT * FROM O_M_Library.dbo.Publisher
GO

