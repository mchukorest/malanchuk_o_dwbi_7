
select * from employee
insert into [employee] (surname,name,post,pharmacy_id)
values ('Maxwell','John','CEO',1)
insert into [employee] (surname,name,post,pharmacy_id)
values ('Foxtrot','Max','pharmacist',1)
insert into [employee] (surname,name,post,pharmacy_id)
values ('Wishly','Micke','pharmacist',1)
insert into [employee] (surname,name,post,pharmacy_id)
values ('McCall','Jack','pharmacist',2)
insert into [employee] (surname,name,post,pharmacy_id)
values ('Lee','Kim','cleaner',1)
insert into [employee] (surname,name,post,pharmacy_id)
values ('Lee','Kim','cleaner',2)
---------------------------------------------------------------------------

insert into [medicine] (name,ministry_code,recipe,narcotic,psychotropic)
values 
('Ampligen','hd-100-20',1,0,0),
('Arbidol','ad-201-31',0,1,0),
('Entecavir','ad-335-65',0,0,1),
('Imiquimod','ad-158-92',1,1,0),
('Norvir','kd-325-10',0,1,1),
('Ritonavir','ed-301-12',1,0,1),
('Stavudine','ld-950-06',0,0,0),
('Zalcitabine','bd-207-30',1,1,1)
--------------------------------------------------------------------------
insert into medicine_zone (medicine_id,zone_id)
values
('1','2'),
('1','3'),
('2','3'),
('2','4'),
('3','2'),
('3','3'),
('3','4'),
('4','2'),
('5','7'),
('6','1'),
('7','6'),
('8','8')
---------------------------------------------------------------------------
insert into [pharmacy] (name,building_number,www,work_time,saturday,sunday,street)
values 
('Biomed','3','www.boimed.com','07:59:59.9999999','0','0','street Krakovskaya'),
('DS','10','www.ds.com','07:59:59.9999999','1','0','street Copernicus'),
('3i','2/1','www.iii.com.ua','07:59:59.9999999','1','1','street Doroshenko'),
('Puls','1','www.puls_ph.com','07:59:59.9999999','1','0','street Titara'),
('Medtech','28','www.medtech.med.com','07:59:59.9999999','1','1','Sq. Cathedral'),
('Znahar','100','www.znahar.ua','23:59:59.9999999','1','1','street Ogienka'),
('Atom','13','www.atom.net','23:59:59.9999999','1','0','Prospekt Svobody'),
('medic+','2','www.medic_plus.com','23:59:59.9999999','1','0','Sq. Galitskaya'),
('Goverment �1','33','www.pharm1.gov.ua','23:59:59.9999999','1','1','street Green')
----------------------------------------------------------------------------------------------------
insert into [pharmacy_medicine] (pharmacy_id,medicine_id)
values
('1','1'),
('1','2'),
('1','3'),
('1','4'),
('1','5'),
('1','6'),
('1','7'),
('1','8'),
('1','9'),
('2','1'),
('2','2'),
('2','3'),
('2','4'),
('2','5'),
('2','6'),
('2','7'),
('2','8'),
('2','9'),
('3','1'),
('3','2'),
('3','3'),
('3','4'),
('3','5'),
('3','6'),
('3','7'),
('3','8'),
('3','9'),
('4','1'),
('4','2'),
('4','3'),
('4','4'),
('4','5'),
('4','6'),
('4','7'),
('4','8'),
('4','9'),
('5','1'),
('5','2'),
('5','3'),
('5','4'),
('5','5'),
('5','6'),
('5','7'),
('5','8'),
('5','9'),
('6','1'),
('6','2'),
('6','3'),
('6','4'),
('6','5'),
('6','6'),
('6','7'),
('6','8'),
('6','9'),
('7','1'),
('7','2'),
('7','3'),
('7','4'),
('7','5'),
('7','6'),
('7','7'),
('7','8'),
('7','9'),
('8','1'),
('8','2'),
('8','3'),
('8','4'),
('8','5'),
('8','6'),
('8','7'),
('8','8'),
('8','9'),
('9','1'),
('9','2'),
('9','3'),
('9','4'),
('9','5'),
('9','6'),
('9','7'),
('9','8'),
('9','9')
--------------------------------------------------------------------
insert into post (post)
values('CEO'),
('pharmacist'),
('cleaner')
--------------------------------------------------------------------
insert into street (street)
values
('street Krakovskaya'),
('street Copernicus'),
('street Doroshenko'),
('street Titara'),
('Sq. Cathedral'),
('street Ogienka'),
('Prospekt Svobody'),
('Sq. Galitskaya'),
('street Green')
---------------------------------------------------------------------
insert into zone (name)
values
('heart'),
('intestines'),
('oesophagus'),
('pancreas'),
('kidneys'),
('eye'),
('lungs'),
('brain')
----------------------------------------------------------------------------
