-- ���� (1->�) � ������ �-� ������� ������ <-> ������ ���
Create or alter trigger pharmTOmedKEY ON Pharmacy
After delete, update
AS
IF exists
(Select 1 from deleted where id in (select pharmacy_id from pharmacy_medicine))
begin print ' can`t update, key dependence !'
rollback transaction
end
go


-- ���� (�->1) � ������ �-� ������� ������ <-> ������ ���
Create or alter trigger pharmmedMANYTO ON pharmacy_medicine
After insert, update
AS
IF exists (Select 1 from inserted where medicine_id not in(select medicine_id from medicine))
OR exists (Select 1 from inserted where pharmacy_id not in (select id from pharmacy))
begin print ' can`t update, dependence !'
rollback transaction
end
go

-- ���� (�->1) � ������ �-� ������ ��� <-> ���� ������ 
Create or alter trigger medzoneMANYTO ON medicine_zone
After insert, update
AS
IF exists (Select 1 from inserted where medicine_id not in(select medicine_id from medicine))
OR exists (Select 1 from inserted where zone_id not in (select id from zone))
begin print ' can`t update, dependence !'
rollback transaction
end
go

-- ���� (1->�) � ������ �-� ������ ��� <-> ���� ������
Create or alter trigger medTOzoneKEY ON medicine
After delete, update
AS
IF exists
(Select 1 from deleted where id in (select medicine_id from medicine_zone))
begin print ' can`t update, key dependence !'
rollback transaction
end
go

-- ���� (1->�) � ������ �-� ������ ��� <-> ������
Create or alter trigger medTOpharmKEY ON medicine
After delete, update
AS
IF exists
(Select 1 from deleted where id in (select medicine_id from pharmacy_medicine))
begin print ' can`t update, key dependence !'
rollback transaction
end
go

-- ���� (1->�) � ������ �-� ���� ������ <-> ������ ��� 
Create or alter trigger zoneTOmedKEY ON zone
After delete, update
AS
IF exists
(Select 1 from deleted where id in (select zone_id from medicine_zone))
begin
print ' can`t update, key dependence !'
rollback transaction
end
go

--���� (1->�) � ������ ������ - �����������
Create or alter trigger postTOempKEY ON Post
After delete, update
AS
IF exists
(Select 1 from deleted where post in (select post from employee))
begin
print ' can`t update, key dependence !'
rollback transaction
end
go

--���� (1->�) � ������ ������ - �����������
Create or alter trigger pharmTOempKEY ON Pharmacy
After delete, update
AS
IF exists
(Select 1 from deleted where id in (select pharmacy_id from employee))
begin print ' can`t update, key dependence !'
rollback transaction
end
go

--���� (1->�) � ������ ������ - ������
Create or alter trigger streetTOpharmKEY ON Street
After delete, update
AS
IF exists (Select 1 from deleted where street  in (select street from pharmacy))
begin 
print ' can`t update, key dependence !'
rollback transaction
end
go

--���� (�->1) � ������ ������ - ������ 
Create trigger pharmMANYTOstreet ON pharmacy
After insert, update
AS
IF exists (Select 1 from inserted where street not in(select street from street))
begin print ' can`t update, dependence !'
rollback transaction
end
go

--���� (�->1) � ������ ����������� - ������ ��
--���� (�->1) � ������ ����������� - ������ 
Create or alter trigger EmplMANYTOpost ON Employee
After insert, update
AS
IF exists (Select 1 from inserted where post not in(select post from post))
OR exists (Select 1 from inserted where id not in (select id from pharmacy))
begin print ' can`t update, dependence !'
rollback transaction
end
go