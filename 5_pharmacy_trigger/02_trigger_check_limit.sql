USE [triger_fk_cursor]
GO


-- �������� ������������� ���� � ������� "������" (post)
Create trigger NotModePost ON Post
After update
AS
Print 'You can`t modify post !'
rollback transaction
go


-- �����. ����� �� ���� ������������ ����� ������
Create trigger NotEndNullEmpl ON Employee
After insert, update
AS
IF exists
(Select 1 from inserted where identity_number like '%00')
begin print ' identity_number can`t ending 00'
rollback transaction
end
go

-- ������ �����: �� ����� ��� � , �, + [-] + 3 ����� [-] + 2 �����
Create or alter trigger checkcode ON medicine
After insert, update
AS
IF exists
(Select 1 from inserted where ministry_code LIKE '%M%' or ministry_code LIKE '%P%' OR ministry_code NOT LIKE '%[-][0-9][0-9][0-9][-][0-9][0-9]')
begin print ' can`t include M or P'
rollback transaction
end
go