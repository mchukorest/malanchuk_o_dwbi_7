--************************************** [description]

CREATE TABLE [description] -- ������� � ������ ��������
(
 [id]          INT NOT NULL ,
 [description] NVARCHAR(500) NOT NULL ,
 [composition] NVARCHAR(500) NOT NULL ,
 [image]       IMAGE NOT NULL ,

 CONSTRAINT [PK_description] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [amount_type]

CREATE TABLE [amount_type] -- ������� � ������ ������� �����
(
 [id_amount]   INT NOT NULL ,
 [name_amount] NVARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_amount_type] PRIMARY KEY CLUSTERED ([id_amount] ASC)
);
GO



--************************************** [factory_address]

CREATE TABLE [factory_address] -- ������� � ������ ������ �� �����������
(
 [id]                   INT NOT NULL ,
 [address]              NVARCHAR(70) NULL ,
 [name_factory]         NVARCHAR(70) NULL ,
 [phone_1_manufacturer] NVARCHAR(50) NULL ,
 [phone_2_manufacturer] NVARCHAR(50) NULL ,

 CONSTRAINT [PK_all_country] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [all_supplier]

CREATE TABLE [all_supplier] -- ������� � ������ �������������
(
 [id]                  INT NOT NULL ,
 [first_name_supplier] NVARCHAR(50) NOT NULL ,
 [last_name_supplier]  NVARCHAR(50) NOT NULL ,
 [phone_1_supplier]    NVARCHAR(50) NOT NULL ,
 [phone_2_supplier]    NVARCHAR(50) NULL ,
 [address_supplier]    NVARCHAR(50) NOT NULL ,
 [email_supplier]      NVARCHAR(50) NULL ,

 CONSTRAINT [PK_table_230] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [all_manufacturer]

CREATE TABLE [all_manufacturer] -- ������� � ������ ���������
(
 [id]                   INT NOT NULL ,
 [name_manufacturer]    NVARCHAR(70) NOT NULL ,
 [address_office]       NVARCHAR(70) NOT NULL ,
 [phone_1_manufacturer] NVARCHAR(50) NOT NULL ,
 [phone_2_manufacturer] NVARCHAR(50) NULL ,
 [email_manufacturer]   NVARCHAR(50) NULL ,

 CONSTRAINT [PK_all_creators] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [payment]

CREATE TABLE [payment] -- ������� � ������ ������� ������
(
 [id]         INT NOT NULL ,
 [pay_method] NVARCHAR(50) NOT NULL ,
 [pay_system] NVARCHAR(50) NULL ,

 CONSTRAINT [PK_payment] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [office]

CREATE TABLE [office] -- ������� � ������ ����� �������� �������� ���� ��
(
 [id]             INT NOT NULL ,
 [name_office]    NVARCHAR(50) NOT NULL ,
 [address_office] NVARCHAR(50) NOT NULL ,
 [city]           NVARCHAR(50) NOT NULL ,
 [country]        NVARCHAR(50) NOT NULL ,
 [phone_1_office] NVARCHAR(50) NOT NULL ,
 [phone_2_office] NVARCHAR(50) NULL ,

 CONSTRAINT [PK_office] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [client]

CREATE TABLE [client] -- ������� � �볺�����
(
 [id]                INT NOT NULL ,
 [first_name_client] NVARCHAR(50) NOT NULL ,
 [last_name_client]  NVARCHAR(50) NULL ,
 [phone_1_client]    NVARCHAR(50) NOT NULL ,
 [phone_2_client]    NVARCHAR(50) NULL ,
 [address_client]    NVARCHAR(50) NOT NULL ,
 [email_client]      NVARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_client] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [info_storage]

CREATE TABLE [info_storage] -- ������� � ������ ���� ������
(
 [id]              INT NOT NULL ,
 [storage_account] NVARCHAR(50) NOT NULL ,
 [address]         NVARCHAR(70) NOT NULL ,
 [phone_1]         NVARCHAR(50) NOT NULL ,
 [phone_2]         NVARCHAR(50) NULL ,

 CONSTRAINT [PK_getting] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [address_info]

CREATE TABLE [address_info] -- ������� ������� ��� ���������� ������
(
 [id_address]      INT NOT NULL ,
 [id_manufacturer] INT NOT NULL ,

 CONSTRAINT [FK_266] FOREIGN KEY ([id_address])
  REFERENCES [factory_address]([id]),
 CONSTRAINT [FK_376] FOREIGN KEY ([id_manufacturer])
  REFERENCES [all_manufacturer]([id])
);
GO


--SKIP Index: [fkIdx_266]

--SKIP Index: [fkIdx_376]


--************************************** [product]

CREATE TABLE [product] -- ������� � ����������
(
 [id]              INT NOT NULL ,
 [name_product]    NVARCHAR(50) NOT NULL ,
 [date_made]       DATETIME NOT NULL ,
 [date_expiration] DATE NOT NULL ,
 [bar_code]        NVARCHAR(50) NULL ,
 [description_id]  INT NOT NULL ,

 CONSTRAINT [PK_movie] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [FK_467] FOREIGN KEY ([description_id])
  REFERENCES [description]([id])
);
GO


--SKIP Index: [fkIdx_467]


--************************************** [employer]

CREATE TABLE [employer] -- ������� � ������������
(
 [id]                  INT NOT NULL ,
 [first_name_employer] NVARCHAR(50) NOT NULL ,
 [last_name_employer]  NVARCHAR(50) NOT NULL ,
 [phone_1_employer]    NVARCHAR(50) NOT NULL ,
 [phone_2_employer]    NVARCHAR(50) NULL ,
 [address_employer]    NVARCHAR(50) NOT NULL ,
 [email_employer]      NVARCHAR(50) NOT NULL ,
 [id_office]           INT NOT NULL ,

 CONSTRAINT [PK_employer] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [FK_131] FOREIGN KEY ([id_office])
  REFERENCES [office]([id])
);
GO


--SKIP Index: [fkIdx_131]


--************************************** [storage]

CREATE TABLE [storage] -- ������� ������ ��� ���������
(
 [id_position]      INT NOT NULL ,
 [id_storage]       INT NOT NULL ,
 [id_product]       INT NOT NULL ,
 [quantity_product] INT NOT NULL ,
 [id_amount]        INT NOT NULL ,

 CONSTRAINT [PK_storage] PRIMARY KEY CLUSTERED ([id_position] ASC),
 CONSTRAINT [FK_399] FOREIGN KEY ([id_storage])
  REFERENCES [info_storage]([id]),
 CONSTRAINT [FK_406] FOREIGN KEY ([id_product])
  REFERENCES [product]([id]),
 CONSTRAINT [FK_443] FOREIGN KEY ([id_amount])
  REFERENCES [amount_type]([id_amount])
);
GO


--SKIP Index: [fkIdx_399]

--SKIP Index: [fkIdx_406]

--SKIP Index: [fkIdx_443]


--************************************** [product_category]

CREATE TABLE [product_category] -- ������� � ������ ������� ��������
(
 [id_category]   INT NOT NULL ,
 [id_product]    INT NOT NULL ,
 [name_category] NVARCHAR(50) NOT NULL ,

 CONSTRAINT [PK_product_category] PRIMARY KEY CLUSTERED ([id_category] ASC),
 CONSTRAINT [FK_383] FOREIGN KEY ([id_product])
  REFERENCES [product]([id])
);
GO


--SKIP Index: [fkIdx_383]


--************************************** [supply]

CREATE TABLE [supply] -- ������� � ������ ��������
(
 [id]           INT NOT NULL ,
 [id_product]   INT NOT NULL ,
 [id_supplier]  INT NOT NULL ,
 [supply_price] FLOAT NOT NULL ,
 [supply_date]  DATETIME NOT NULL ,
 [quantity]     INT IDENTITY (1, 1) NOT NULL ,
 [id_amount]    INT NOT NULL ,
 [responsible]  INT NOT NULL ,
 [price]        MONEY NOT NULL ,

 CONSTRAINT [PK_movie_actor] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [FK_226] FOREIGN KEY ([id_product])
  REFERENCES [product]([id]),
 CONSTRAINT [FK_236] FOREIGN KEY ([id_supplier])
  REFERENCES [all_supplier]([id]),
 CONSTRAINT [FK_324] FOREIGN KEY ([responsible])
  REFERENCES [employer]([id]),
 CONSTRAINT [FK_365] FOREIGN KEY ([id_amount])
  REFERENCES [amount_type]([id_amount])
);
GO


--SKIP Index: [fkIdx_226]

--SKIP Index: [fkIdx_236]

--SKIP Index: [fkIdx_324]

--SKIP Index: [fkIdx_365]


--************************************** [maker]

CREATE TABLE [maker] -- ������� ������� ��� ���������� ���������
(
 [id]              INT NOT NULL ,
 [id_product]      INT NOT NULL ,
 [id_manufacturer] INT NOT NULL ,

 CONSTRAINT [PK_movie_creator] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [FK_216] FOREIGN KEY ([id_product])
  REFERENCES [product]([id]),
 CONSTRAINT [FK_309] FOREIGN KEY ([id_manufacturer])
  REFERENCES [all_manufacturer]([id])
);
GO


--SKIP Index: [fkIdx_216]

--SKIP Index: [fkIdx_309]


--************************************** [order]

CREATE TABLE [order] -- ������� � ��������� ����������
(
 [id]                INT NOT NULL ,
 [id_client]         INT NOT NULL ,
 [id_employer]       INT NOT NULL ,
 [id_product]        INT NOT NULL ,
 [id_payment_method] INT NOT NULL ,
 [quantity]          INT NOT NULL ,
 [id_amount]         INT NOT NULL ,
 [price]             MONEY NOT NULL ,
 [order_date]        DATETIME NOT NULL ,

 CONSTRAINT [PK_order] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [FK_121] FOREIGN KEY ([id_employer])
  REFERENCES [employer]([id]),
 CONSTRAINT [FK_141] FOREIGN KEY ([id_product])
  REFERENCES [product]([id]),
 CONSTRAINT [FK_164] FOREIGN KEY ([id_payment_method])
  REFERENCES [payment]([id]),
 CONSTRAINT [FK_427] FOREIGN KEY ([id_amount])
  REFERENCES [amount_type]([id_amount])
);
GO


--SKIP Index: [fkIdx_121]

--SKIP Index: [fkIdx_141]

--SKIP Index: [fkIdx_164]

--SKIP Index: [fkIdx_427]


--************************************** [log_order]

CREATE TABLE [log_order] -- ������� � ������� ���������
(
 [id]          INT NOT NULL ,
 [id_order]    INT NOT NULL ,
 [id_client]   INT NOT NULL ,
 [id_employer] INT NOT NULL ,

 CONSTRAINT [PK_log_order] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [FK_274] FOREIGN KEY ([id_order])
  REFERENCES [order]([id]),
 CONSTRAINT [FK_278] FOREIGN KEY ([id_client])
  REFERENCES [client]([id]),
 CONSTRAINT [FK_282] FOREIGN KEY ([id_employer])
  REFERENCES [employer]([id])
);
GO